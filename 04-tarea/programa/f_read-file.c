// leer el archivo
int read_file(char * file){
    FILE * fp;
    char * line = NULL;
    char number[4];
    size_t len = 0;
    ssize_t read;
    int i, j, pos, ptr;
    

    fp = fopen(file, "r");
    if (fp == NULL){
        printf("No se encuentra el archivo");
        exit(EXIT_FAILURE);
    }
    
    // lee la primera linea y reserva los bloques de memoria para flow y distance
    if ((read = getline(&line, &len, fp)) == -1){
        printf("Error leyendo el archivo");
        exit(EXIT_FAILURE);
    }

    size = atoi(line);
    memory_allocation_matrix();       
    
    // lee y descarta la linea vacia
    read = getline(&line, &len, fp);
    
    // leer la primera matriz "distance"
    pos = 0;    
    while ((read = getline(&line, &len, fp)) != -1 && read > 1) {                                                
        for(i = 0, ptr = 0; i <= read; i++){                
            if(line[i] == ' '){                
                distance[pos++] = atoi(number);                
                for(int j = 0; j < ptr; j++){ 
                    number[j] = '\0';
                }   
                ptr = 0;                
            }else{                                                        
                if(line[i] >= 48 && line[i] <= 59)
                    number[ptr++] = line[i];
            }
        }            
    }    

    // leer la segunda matriz "flow"
    pos = 0;    
    while ((read = getline(&line, &len, fp)) != -1 && read > 1) {                                                
        for(i = 0, ptr = 0; i <= read; i++){                
            if(line[i] == ' ' ){                
                flow[pos++] = atoi(number);                
                for(int j = 0; j < ptr; j++){ 
                    number[j] = '\0';
                }   
                ptr = 0;                
            }else{                                                        
                if(line[i] >= 48 && line[i] <= 59)
                    number[ptr++] = line[i];
            }
        }            
    }            

    fclose(fp);
    if (line)
        free(line);
    
}