/* Global structures and variables */

struct individual {
    unsigned *chrom;             /*      chromosome string for the individual */    
    long int fitness;            /* fitness of the individual in this case the evaluation costo of the individual */        
    long int cost;            /* fitness of the individual in this case the evaluation costo of the individual */        
    int    parent[2];            /*         who the parents of offspring were */
};

struct best_ind{
    unsigned *chrom;        /* chromosome string for the best_ind-ever individual */    
    long int fitness;       /* fitness of the individual in this case the evaluation costo of the individual */            
    long int cost;          /*             the evaluation costo of the individual */
    int      generation;    /*                       generation which produced it */
};

//------------------------
/* Functions prototypes */
//------------------------
void memory_allocation_matrix();
void memory_allocation();
void no_memory(char *);
void free_all();

void initialize_pop();
void create_generation();
void statistics(struct individual *);
int selection();
void crossover(int, int, int, int);
void mutation(struct individual *);
void inversion(struct individual *);
void obj_func(struct individual *);

void copy_bits(unsigned *, unsigned *, int, int);
void shuffle();
void generate_permutation(unsigned *, int);
int verify_unique_chrom(unsigned *, int);
void scan_float(char *, float *, float, float);

// report and console functions
void cls();
void initial_report();
void generation_report();
void best_ever_report();
void write_pop();
void write_line(int, struct individual *);
void write_chrom(unsigned *);
void write_best(struct best_ind ind);

// random functions
void warmup_random(float);
float rnd_real(float,float);
int rnd_int(int,int);
float random_perc();
double random_normal_deviate();
void randomize();
double noise(double,double);
void init_random_normal_deviate();
void advance_random();
int flip(float);

// read file functions
int read_file(char *);

//------------------------
//       variables
//------------------------

// control and program variables
int size; /* the problem size, also know as n */
int gen; /* current generation number */
unsigned *flow; /* flow matrix */
unsigned *distance; /* distance matrix */
unsigned *temp; /* temporal vector for copy parent bits  */
int *perm_vector; /* vector for verify permutations  */
struct individual *old_pop; /* last generation of individuals */
struct individual *new_pop; /* next generation of individuals */
static int *tourn_list, tourn_pos, tourn_size; /* Tournment list, position in list */

// statistics variables
struct best_ind best_ind_gen; /* fittest individual per generation */
struct best_ind best_ever; /* fittest individual so far */
double fitness_sum; /* summed fitness for entire population */
double fitness_max; /* maximum fitness of population */
double fitness_min; /* minimum fitness of population */
double avg; /* average fitness of population */
int n_mutation; /* number of mutations */
int n_cross; /* number of crossovers */
int n_inversion; /* number of inversions */

// arguments
int Gmax; /* maximum number of generations */
int pop_size; /* population size */
float Pc; /* probability of crossover */
float Pm; /* probability of mutation */
float Pi; /* probability of inversion */
int print_arg; /* boolean for print or not to print population */

// random variables
float r_seed;  /* Seed for generate random numbers */
double oldrand[55]; /* Array of 55 random numbers */
int jrand; /* current random number */
double rndx2; /* used with random normal deviate */
int rndcalcflag; /* used with random normal deviate */

