/* Allocate memory for matrix flow and distance */
void memory_allocation_matrix(){
    /* Allocate memory  */
    unsigned numbytes ; 
    
    numbytes = size*size * sizeof(unsigned *);

    if ((flow = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("flow matrix");
    if ((distance = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("distance matrix");
}

/* Perform the memory allocation needed for the strings that will be generated */
void memory_allocation() {
    unsigned numbytes ;        

    /* Allocate memory for old and new populations of individuals */    
    numbytes = pop_size * sizeof(struct individual);

    if ((old_pop = (struct individual *) malloc(numbytes)) == NULL)
        no_memory("old population");

    if ((new_pop = (struct individual *) malloc(numbytes)) == NULL)
        no_memory("new population");

    /* Allocate memory for tournament */    
    numbytes = pop_size*sizeof(int);	
    if ((tourn_list = (int *) malloc(numbytes)) == NULL)
		no_memory("tournament list");

    /* Allocate memory for chromosome strings in populations */
    numbytes = size * sizeof(unsigned *);
    for (int i = 0; i < pop_size; i++) {
        if ((old_pop[i].chrom = (unsigned *) malloc(numbytes)) == NULL)
            no_memory("old population chromosomes");

        if ((new_pop[i].chrom = (unsigned *) malloc(numbytes)) == NULL)
            no_memory("new population chromosomes");
    }

    if ((best_ind_gen.chrom = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("best_ind_gen chromosomes");

    if ((best_ever.chrom = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("best_ever chromosomes");

    /* Allocate memory for permutation vector */ 
    numbytes = size * sizeof(int);    
    if ((perm_vector = (int *) malloc(numbytes)) == NULL)
        no_memory("Permutation verification");
	
}

/* When done, free all memory */
void free_all() {    
    for (int i = 0; i < pop_size; i++) {
        free(old_pop[i].chrom);
        free(new_pop[i].chrom);
    }
    free(old_pop);
    free(new_pop);    
    free(best_ever.chrom);
    free(flow);
    free(distance);
    free(perm_vector);
    
}

/* Notify if we run out of memory when generating a chromosome */
void no_memory(char *string) {
    printf("ERROR!! --> malloc: out of memory making %s\n", string);
    exit(1);
}