/*============================================================================= 
 |
 |       Author:  Antonio Leyva
 |     Language:  C
 |
 |   To Compile:  gcc genetic.c -o genetic.out -lm  					
 |
 |   To execute (no parameters):  					
                    ./genetic.out
 |   To execute (with parameters):  					 
 					./genetic.out 500 1000 .4 .1 .1 0 ./datos/tai12.dat
 
 |        Class:  Computación Evolutiva
 |   Instructor:  Carlos Coello
 |     Due Date:  01-jul-2019
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Programa algoritmo genético para resolver el problema de asignacion cuadrática
 				costo = ∑from 0 to N ∑ from 0 to N ( distance[i][j] × flow[x[i]][x[j]] ) 
            
           With:  Cruza: Order-based Crossover                  
                  Mutación: por Intercambio Recı́proco
                  Remoción de duplicados
                  Selección: torneo binario determinı́stico

 |
 *===========================================================================*/


// best parameters ./genetic.out 500 1000 .4 .1 .1 0 ./datos/tai12.dat
// watch info ./genetic.out 3 10 .4 .1 .1 3 ./datos/tai12.dat

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>   
#include "genetic.h"
#include "f_random.c"
#include "f_memory.c"
#include "f_read-file.c"
#include "f_report.c"


int main(int argc, char **argv) {
    cls();
    char *file;

    if(argc != 8 && argc != 1){
        printf("\nERROR\nParámetros incorrectos: <Gmax> <Pop_size> <Pc> <Pm> <Pi> <print_arg> <input_file>\n");
        printf(" - Gmax : maximum number of generations\n");
        printf(" - Pop_size : population size\n");        
        printf(" - Pc : probability of crossover\n");        
        printf(" - Pm : probability of mutation\n");        
        printf(" - Pi : probability of inversion\n");        
        printf(" - print_arg : \n");                
        printf("\t3 for print all details; all generation and statistics\n");
        printf("\t2 for print statistics in each generation (omit the generation detail)\n");
        printf("\t1 for print only the best individual of each generation\n");        
        printf("\t0 for print only best ever individual\n");
        printf(" - input_file : the input file (with path), contains the flow and distance matrix  \n");        
        exit(1);
    }

    if(argc == 8){
        Gmax = atoi(argv[1]); // reading the maximum number of generations        
        pop_size = atoi(argv[2]); /* Population size */     
        Pc = atof(argv[3]); // reading the probability of crossover     
        Pm = atof(argv[4]); // reading mutation rate 
        Pi = atof(argv[5]); // reading mutation rate 
        print_arg = atoi(argv[6]); // reading print report parameter */           
        /* read file and perform the allocate and store matrix data for flow and distance */
        file = argv[7];        
        //random seed
        srand((unsigned)time(NULL));
        r_seed = (float) rand() / (RAND_MAX);
    }else{
        scan_float("random seed",&r_seed,0,1);         
        printf("Enter the maximum number of generations ---------> "); scanf("%d",&Gmax);
        printf("Enter the population size ---------> "); scanf("%d",&pop_size);        
        scan_float("probability of crossover",&Pc,0,1);
        scan_float("mutation rate",&Pm,0,1);
        scan_float("probability of inversion",&Pi,0,1);         
        printf("Enter the evaluation file (with path)  --------------> "); scanf("%s",file);                   
        printf("Enter the printing format (suggested \"2\")\n");
        printf("\t3 for print all details; all generation and statistics\n");
        printf("\t2 for print statistics in each generation (omit the generation detail)\n");
        printf("\t1 for print only the best individual of each generation\n");        
        printf("\t0 for print only best ever individual\n");
        printf("------------> ");         
        scanf("%d",&print_arg);
        
    }

    // read the file and load flow and distance matrix.
    read_file(file);        

    /* Perform the memory allocation */
    memory_allocation();

    /* Initialize random number generator */
    randomize();

    // variables initialization    
    best_ever.fitness = 0;
    best_ever.generation = 0;
    tourn_size = 2;  /* Use binary tournament selection */

    /* Initialize the populations and report statistics */
    initialize_pop();    
    statistics(old_pop);
    initial_report();

    struct individual *temp;    
    for (gen = 1; gen <= Gmax; gen++){ // Iterate the given number of generations  {
        // Create a new generation 
        create_generation();
        // Compute fitness statistics on new populations 
        statistics(new_pop);
        // Report results for new generation 
        generation_report();
        // Advance the generation 
        temp = old_pop;
        old_pop = new_pop;
        new_pop = temp;
    }

    //report the best individual
    best_ever_report();    

    free_all();
    return 0;
}

/* Define randomly the population for generation # 0 */
void initialize_pop() {    
    int i;    

    for (i = 0; i < pop_size; i++) {                
        // generate chromosome and iterate until is a unique chromosome
        do{
            generate_permutation(old_pop[i].chrom, size);
        }while(verify_unique_chrom(old_pop[i].chrom, i));

        obj_func( &old_pop[i] );
        old_pop[i].parent[0] = 0; /* Initialize parent info */
        old_pop[i].parent[1] = 0;        
    }

    // initialize best_ever
    best_ever.chrom = old_pop[0].chrom;    
    best_ever.fitness = old_pop[0].fitness;    
    best_ever.cost = old_pop[0].cost;    
}

/* Perform all the work involved with the creation of a new generation of chromosomes */
void create_generation() {
    int i, mate1, mate2;
    
    /* initialize crossover, mutation and inversion counters */
    n_mutation = 0;
    n_cross = 0;
    n_inversion = 0;

    /* perform any preselection actions necessary before generation */
    /* Perform a preselection */
	shuffle();
	tourn_pos = 0;
    
    // crossover and mutation
    for(i = 0; i < pop_size - 1; i+=2){            
        /* pick a pair of mates */        
        mate1 = selection();
        mate2 = selection();

        // /* Crossover */
        crossover( mate1, mate2, i, i+1 );

        // /* mutation */
        mutation( &new_pop[i] );
        mutation( &new_pop[i+1] );

        inversion( &new_pop[i] );
        inversion( &new_pop[i+1] );
    }

    //Elitism, replace the best individual of the last generation into the new one
    copy_bits(new_pop[0].chrom, best_ind_gen.chrom, 0, size);            
    obj_func( &new_pop[0] );

    // verificar que cada cromosoma sea una unica permutacion dentro de la poblacion
    for (i = 0; i < pop_size; i++) {  
        for(;verify_unique_chrom(old_pop[i].chrom, i);)
            generate_permutation(old_pop[i].chrom, size);        
        // /* Evaluate fitness and cost (obj_func) */        
        obj_func( &new_pop[i] );
    }
}

/* Calculate population statistics */
void statistics(struct individual *pop) {
    
    fitness_sum = 0.0;
    fitness_min = pop[0].fitness;
    fitness_max = pop[0].fitness;
    best_ind_gen.chrom = pop[0].chrom;
    best_ind_gen.fitness = pop[0].fitness;

    /* Loop for max, min */
    for (int i = 0; i < pop_size; i++) {
        fitness_sum = fitness_sum + pop[i].fitness; /* Accumulate */        
        
        if (pop[i].fitness > fitness_min){
            fitness_min = pop[i].fitness; /* New minimum */            
        }
        if (pop[i].fitness < fitness_max) {
            fitness_max = pop[i].fitness; /* New maximum */                                   
            best_ind_gen.chrom = pop[i].chrom;
            best_ind_gen.fitness = pop[i].fitness;            
            best_ind_gen.cost = pop[i].cost;            

            /* Define new global best_ever-fit individual */
            if (pop[i].fitness < best_ever.fitness) {                
                copy_bits(best_ever.chrom, pop[i].chrom, 0, size);
                best_ever.fitness = pop[i].fitness;                
                best_ever.cost = pop[i].cost; 
                best_ever.generation = gen;
            } 
        }
    }
    
    /* Loop for computate each chomosome percent (for rulette purpose) */               
    for (int i = 0; i < pop_size; i++) {
        //pop[i].percent = pop[i].fitness / fitness_sum;
    }

    /* Calculate average */
    avg = fitness_sum / pop_size;
}

/* Implementation of a tournament selection process */
int selection(){    
    int pick, winner, i;    

	/* If remaining members not enough for a tournament, then reset list */
	if ((pop_size - tourn_pos) < tourn_size){
		shuffle();
		tourn_pos = 0;
	}

	/* Select tournament size structures at random and conduct a tournament */
	winner = tourn_list[tourn_pos];

	for (i=1; i < tourn_size; i++){
		pick=tourn_list[i+tourn_pos];
		if(old_pop[pick].fitness < old_pop[winner].fitness) 
            winner=pick;
	}

	/* Update tournament position */
	tourn_pos += tourn_size;
	return(winner);
}

/*Order-Based-crossover 
1. Seleccionar (al azar) un conjunto de posiciones de P1 (no necesariamente consecutivas)
2. removemos esos valores de P2
3. generamos un hijo a partir de P2’.
 */
void order_based_crossover(struct individual *child, struct individual *parent1, struct individual *parent2){
    int i, j, k;
    //int lenght = rnd_int(2, (int)size/2); // generar un numero aleatorio para seleccionar elementos, en rango de 2 a size/2.
    int lenght = (int)size/2; // define el numero para seleccionar elementos, en rango de 2 a size/2.
    unsigned selection[lenght];

    // Seleccionar (al azar) un conjunto de posiciones de P1 (no necesariamente consecutivas)
    generate_permutation(selection, lenght);

    // limpiar el cromosoma del hijo
    for (i = 0; i < size; i++){
        child->chrom[i] = -1;        
    }
    
    // copiar los valores de padre2 al hijo
    copy_bits(child->chrom, parent2->chrom, 0, size);

    // Removamos del hijo, los valores seleccionados
    for (i = 0; i < size; i++){
        for (j = 0; j < lenght; j++){
            if(child->chrom[i] == selection[j]){
                child->chrom[i] = -1;
                break;
            }
        }            
    }
    
    // Insertamos ahora la secuencia elegida de P1:
    // se recorre todo el cromosoma del hijo para rellenar las posiciones donde falta valor
    for (i = 0, k = 0; i < size; i++){
        if(child->chrom[i] == -1){
            // se recorre todo el cromosoma del padre para buscar los numeros faltantes en el hijo
            for (j = 0; j < size; j++){
                // se recorre la seleccion para validar si ese elemento del padré es parte de la seleccion
                // y así copiarlo al hijo.
                for ( k = 0; k < lenght; k++){
                    if(selection[k] != -1){
                        if(parent1->chrom[j] == selection[k] ){
                            child->chrom[i] = parent1->chrom[j];
                            selection[k] = -1;
                            break;
                        }
                    }
                }                    
                if(k < lenght)
                    break;
            }
        }       
    }
}

/* Cross 2 parent strings, place in 2 child strings */
void crossover( int parent1_idx, int parent2_idx, int child1_idx, int child2_idx){    
    
    struct individual *child1 = &new_pop[child1_idx];
    struct individual *child2 = &new_pop[child2_idx];    
    struct individual *parent1 = &old_pop[parent1_idx];
    struct individual *parent2 = &old_pop[parent2_idx];

    /* Do crossover with probability Pc */
    if (flip(Pc)) {        
        order_based_crossover(child1, parent1, parent2);
        order_based_crossover(child2, parent2, parent1); 
        child1->parent[0] = child2->parent[0] = parent1_idx;
        child1->parent[1] = child2->parent[1] = parent2_idx;    

        n_cross++; // increase the mutation count.       
    } else {          
        // copy exactly, parents to children
        copy_bits( child1->chrom, parent1->chrom, 0, size );
        copy_bits( child2->chrom, parent2->chrom, 0, size );
        child1->parent[0] = child2->parent[0] = -1;
        child1->parent[1] = child2->parent[1] = -1;            
    }          
}


// Mutación por Intercambio Recı́proco
// two random points are selected and these values ​​are exchanged
// position.
// Example, origin:
// P =  9 4 2 1 5 7 6 10 3 8
// Goal:
// P’ = 9 10 2 1 5 7 6 4 3 8
void mutation(struct individual *ind){
    /* Do mutation with probability Pm */
    if (flip(Pm)){
        int i, j;
        int lenght = 2;
        unsigned selection[lenght]; //vector que va a obtener dos posiciones al azar.

        // Select (at random) a set of two positions (not necessarily consecutive)
        generate_permutation(selection, 2);
        
        // exchange the elements in pos1 and pos2
        unsigned temp = ind->chrom[selection[0]];
        ind->chrom[selection[0]] = ind->chrom[selection[1]];
        ind->chrom[selection[1]] = temp;

        n_mutation++; // increase the mutation count.
    }
}

// inversion choose 2 points randomly points and invert the order of all the genes between 2 selected points
void inversion(struct individual *ind){
    /* Do crossover with probability Pi */
    if (flip(Pi)){
        int i,j;
        unsigned temp;             
        unsigned selection[2]; //vector que va a obtener dos posiciones al azar.

        // Select (at random) a set of two positions (not necessarily consecutive)
        generate_permutation(selection, 2);                

        // arrange the points
        if(selection[0] > selection[1]){
            j = selection[0];
            i = selection[1];
        }else{
            i = selection[0];
            j = selection[1];
        }       

        for (; i < j; i++, j--){
            temp = ind->chrom[i];
            ind->chrom[i] = ind->chrom[j];
            ind->chrom[j] = temp;
        }

        n_inversion++; // increase the inversion count.
    }
}

/* Define the objective function. In this case, 
costo = ∑from 0 to N ∑ from 0 to N ( distance[i][j] × flow[x[i]][x[j]] ) */
void obj_func(struct individual *ind){    
    long int cost = 0;
    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            cost += distance[i*size+j] * flow[ind->chrom[i]*size+ind->chrom[j]];
        }        
    }            
    ind->cost = cost;
    ind->fitness = cost;
}




//---------------------------------
//       Auxiliar functions
//---------------------------------

/* Shuffle the tournament list at random */
void shuffle(){
	int i, rand1, rand2, temp;
	
    for (i=0; i < pop_size; i++) 
        tourn_list[i]=i;

	for (i=0; i < pop_size; i++){
		rand1=rnd_int(i,pop_size-1);
		rand2=rnd_int(i,pop_size-1);
		temp=tourn_list[rand1];
		tourn_list[rand1]=tourn_list[rand2];
		tourn_list[rand2]=temp;
	}
}

/* Copy a range of allel, from source to destiny */
void copy_bits(unsigned *dest, unsigned *source, int begin, int end){
    for (int i = begin; i < end; i++){
        dest[i] = source[i];
    }    
}

/* Generate a random permutation in perm_vector of size "length" in range N */
void generate_permutation(unsigned * chrom, int length){
    int i, j, k;
    int rand_num, number, remaining;
    
    // reset the perm_vector (store the sorted permutation {1,2,...,N} )
    for (int i = 0; i < size; i++) {  
        perm_vector[i] = i;
    }          
    
    remaining = size;    
    for (i = 0; i < length; i++, remaining--){
        rand_num = (unsigned)rnd_int(1, remaining);
        for (j = 0, k = 0; k < rand_num; j++){
            if(perm_vector[j] != -1)
                k++;
        }        
        chrom[i] = perm_vector[j-1];
        perm_vector[j-1] = -1;
    }    
}

// verify that the generated chromosome is unique throughout the "old_pop", 
// up to the "pop_idx" index
int verify_unique_chrom(unsigned * chrom, int pop_idx){
    int i, j;    
    for (i = 0; i < pop_idx; i++) {          
        for(j = 0; old_pop[i].chrom[j] == chrom[j] && j < size; j++);
        if(size == j)
            return 1;
    }
    return 0;
}

void scan_float(char *name, float *value, float from_v, float to_v){           
    do{
        printf("Enter the %s, in range %3.1f to %3.1f ------------> ", name, from_v, to_v);
        scanf("%f", value);
    }while( *value < from_v || *value > to_v );
}