
/* Print the initial report of the parameters given by the user */
void initial_report() {
    printf("\nParameters used with the Genetic Algorithm \n");
    printf(" random seed                    =   %3.2f\n", r_seed);    
    printf(" Problem size (n)               =   %d\n", size);
    printf(" Maximum number of generations  =   %d\n", Gmax);
    printf(" Total population size          =   %d\n", pop_size);        
    printf(" Crossover probability          =   %3.2f\n", Pc);
    printf(" Mutation probability           =   %3.2f\n", Pm);    
    printf(" Inversion probability          =   %3.2f\n\n", Pi);    

}

/* Write the population report on the screen */
void generation_report(){
    if(print_arg >= 3){               
        printf("____________________________________________________________________________________\n");
        printf("                    P O P U L A T I O N          R E P O R T  \n");
        printf(" Generation # %3d \t\t\t\t Generation # %3d\n", gen-1, gen);
        printf(" num  chromosome      cost    fitness | parents   chromosome       cost    fitness\n");        
        printf("____________________________________________________________________________________\n\n");

        write_pop();
    }

    /* write the summary statistics in global mode  */    
    if(print_arg >= 1){        
        printf("____________________________________________________________________________________\n");
        printf("Resume Generation # %d\n", gen);
        if(print_arg >= 2){
            printf("  Accumulated Statistics:\n");          
            printf("    Total Crossovers = %d, Total Mutations = %d\n", n_cross, n_mutation);
            printf("    min = %8.1lf   max = %8.1lf   avg = %8.1lf   sum = %8.1lf\n", fitness_min, fitness_max, avg, fitness_sum);
        }
        printf("  Best individual in this generation\n"); 
        write_best(best_ind_gen); printf("\n");       
    }else{
        if(gen%100 == 0)
            printf("Generation # %d\n", gen);
    }
}

void best_ever_report(){    
    printf("\n|-----------------------------------------------------------------|\n");    
    printf("   Global best Individual so far, Generation # %d: \n", best_ever.generation);
    write_best( best_ever );     
    printf("|-----------------------------------------------------------------|\n\n");    

}

/* Give the appropriate format to the several values that get printed */
void write_pop() {
    struct individual *pind;
    int j;
    int type_of_print = 1; 

    for (j = 0; j < pop_size; j++) {
        printf("%3d) ", j);

        // Old string 
        pind = & (old_pop[j]);            
        write_line(type_of_print, pind);

        // New string 
        pind = &(new_pop[j]);            
        printf("(%3d,%3d) ", pind->parent[0], pind->parent[1]);
        write_line(type_of_print, pind); 

        printf("\n");
    }
}

void write_line(int type_of_print, struct individual *pind){
    switch(type_of_print){
        case 1: 
            write_chrom(pind->chrom);
            printf(" %8ld %8ld | ", pind->fitness, pind->cost);
            break;                        
    }
}

/* Print out each chromosome from the most significant bit to the least significant bit */
void write_chrom(unsigned *chrom){	
	for (int i = 0; i<size; i++) {
		printf("%d.",chrom[i]);			
	}
}

void write_best(struct best_ind ind){    
    printf("    ");     
    write_chrom(ind.chrom);    
    printf("    Fittest = %6ld   Cost = %6ld\n", ind.fitness, ind.cost);    
}

/* Use a system call to clear the screen */
void cls(){
    system("clear");
}
