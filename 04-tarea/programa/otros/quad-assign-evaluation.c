#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
//#include "read-file.c"

unsigned *flow;
unsigned *distance;
unsigned *sol;
int size;

int obj_func(){
    int costo = 0;
    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            costo += distance[i*size+j] * flow[sol[i]*size+sol[j]];
        }        
    }    
    return costo;
}


// 
void allocate_memory(int size){
    /* Allocate memory  */
    unsigned numbytes ; 
    numbytes = size*size * sizeof(unsigned *);

    flow = (unsigned *) malloc(numbytes);
    distance = (unsigned *) malloc(numbytes);
}

// leer el archivo
int read_file(char * file){
    FILE * fp;
    char * line = NULL;
    char number[4];
    size_t len = 0;
    ssize_t read;
    int i, j, pos, ptr;
    

    fp = fopen(file, "r");
    if (fp == NULL){
        printf("No se encuentra el archivo");
        exit(EXIT_FAILURE);
    }

    
    // lee la primera linea y reserva los bloques de memoria para flow y distance
    if ((read = getline(&line, &len, fp)) != -1)
        size = atoi(line);
        allocate_memory(size);       
    
    // lee y descarta la linea vacia
    read = getline(&line, &len, fp);
    
    // leer la primera matriz
    pos = 0;    
    while ((read = getline(&line, &len, fp)) != -1 && read > 1) {                                                
        for(i = 0, ptr = 0; i <= read; i++){                
            if(line[i] == ' '){                
                distance[pos++] = atoi(number);                
                for(int j = 0; j < ptr; j++){ 
                    number[j] = '\0';
                }   
                ptr = 0;                
            }else{                                                        
                if(line[i] >= 48 && line[i] <= 59)
                    number[ptr++] = line[i];
            }
        }            
    }    

    // leer la segunda matriz
    pos = 0;    
    while ((read = getline(&line, &len, fp)) != -1 && read > 1) {                                                
        for(i = 0, ptr = 0; i <= read; i++){                
            if(line[i] == ' ' ){                
                flow[pos++] = atoi(number);                
                for(int j = 0; j < ptr; j++){ 
                    number[j] = '\0';
                }   
                ptr = 0;                
            }else{                                                        
                if(line[i] >= 48 && line[i] <= 59)
                    number[ptr++] = line[i];
            }
        }            
    }        
    
     printf("\nDistance\n");
    for(i = 0; i < size*size; i++)
        printf("%d ", distance[i]);

    printf("\nFLOW\n");
    for(i = 0; i < size*size; i++)
        printf("%d ", flow[i]);

    fclose(fp);
    if (line)
        free(line);
    
}

int main(int argc, char* argv){
    
    read_file("../datos/ajuste.dat");    

    /* Allocate memory  */
    unsigned numbytes ; 
    numbytes = size * sizeof(unsigned *);
    sol = (unsigned *) malloc(numbytes);
    sol[0] = 2;
    sol[1] = 3;
    sol[2] = 4;
    sol[3] = 0;
    sol[4] = 1;
    
    printf("\ncosto %d\n", obj_func());

}