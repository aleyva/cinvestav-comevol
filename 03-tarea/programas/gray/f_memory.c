
/* Perform the memory allocation needed for the strings that will be generated */
void memory_allocation() {
    unsigned numbytes ;       

    // Allocate memory for binary chomosome  
    numbytes = BITSVAR * sizeof(unsigned *);
    
    if ((binary = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("binary chromosome");     
    
    /* Allocate memory for old and new populations of individuals */    
    numbytes = pop_size * sizeof(struct individual);

    if ((old_pop = (struct individual *) malloc(numbytes)) == NULL)
        no_memory("old population");

    if ((new_pop = (struct individual *) malloc(numbytes)) == NULL)
        no_memory("new population");

    if ((temp_pop = (struct individual *) malloc(numbytes)) == NULL)
        no_memory("temp population");

    /* Allocate memory for chromosome strings in populations */
    numbytes = bits_chrom * sizeof(unsigned *);
    for (int i = 0; i < pop_size; i++) {
        if ((old_pop[i].chrom = (unsigned *) malloc(numbytes)) == NULL)
            no_memory("old population chromosomes");

        if ((new_pop[i].chrom = (unsigned *) malloc(numbytes)) == NULL)
            no_memory("new population chromosomes");
    }

    if ((temp_child1_chrom = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("binary chromosome"); 
    if ((temp_child2_chrom = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("binary chromosome"); 

    if ((best_ind_gen.chrom = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("best_ind_gen chromosomes");

    if ((best_ever.chrom = (unsigned *) malloc(numbytes)) == NULL)
        no_memory("best_ever chromosomes");
}

/* When done, free all memory */
void free_all() {    
    for (int i = 0; i < pop_size; i++) {
        free(old_pop[i].chrom);
        free(new_pop[i].chrom);
    }
    free(old_pop);
    free(new_pop);
    free(temp_pop);
    free(binary);  
    free(temp_child1_chrom);
    free(temp_child2_chrom);  
}

/* Notify if we run out of memory when generating a chromosome */
void no_memory(char *string) {
    printf("ERROR!! --> malloc: out of memory making %s\n", string);
    exit(1);
}