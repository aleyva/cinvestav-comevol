

#define BITSVAR 17 // Number of bits used per variable of the function. 
                    //In this problems, there are 4 variables
#define VAR 2       // number of variables

/* Global structures and variables */

struct individual {
    unsigned *chrom;             /*      chromosome string for the individual */
    double x1;			         /*	          value of the decoded string */
    double x2;			         /*	          value of the decoded string */    
    double fitness;              /* 	            fitness of the individual */
    double val_esp;
    int    cross_point;          /* 	           crossover site 1 at mating */                    
    int    parent[2];            /*         who the parents of offspring were */
};

struct best_ind{
    unsigned *chrom;        /* chromosome string for the best_ind-ever individual */
    double x1;			    /*	          value of the decoded string */
    double x2;			    /*	          value of the decoded string */    
    double   fitness;       /*            fitness of the best_ind-ever individual */
    int      generation;    /*                   generation which produced it */
};

/* Functions prototypes */

void initialize_pop();
void create_generation();
void statistics(struct individual *);
int selection();
void mutation(struct individual *);
void crossover(int, int);
void obj_func(struct individual *);
int flip(float);
void copy_bits(unsigned *, unsigned *, int, int);
unsigned change(unsigned c);
void gray_to_binary(unsigned *);
int decode(struct individual *);

// report and console functions
void cls();
void initial_report();
void generation_report();
void best_ever_report();
void write_pop();
void write_line(int, struct individual *);
void write_chrom(unsigned *);
void write_best(struct best_ind ind);

// random functions
void warmup_random(float);
float rnd_real(float,float);
int rnd_int(int,int);
float random_perc();
double random_normal_deviate();
void randomize();
double noise(double,double);
void init_random_normal_deviate();
void advance_random();

// memory functions
void memory_allocation();
void no_memory(char *);
void free_all();

// sort functions
void merge(struct individual *, int, int, int );
void merge_sort(struct individual *, int, int );


//------------------------//
//      VARIABLES         //
//------------------------//

//genetic variables
struct individual *old_pop; /* last generation of individuals */
struct individual *new_pop; /* next generation of individuals */
struct individual *temp_pop; /* next generation of individuals */
unsigned *temp_child1_chrom;
unsigned *temp_child2_chrom;
struct best_ind best_ind_gen; /* fittest individual per generation */
struct best_ind best_ever; /* fittest individual so far */
double valesp_sum; /* summed fitness for entire population */
double avg; /* average fitness of population */
int gen; /* current generation number */
int Gmax; /* maximum number of generations */
int pop_size; /* population size */
float Pc; /* probability of crossover */
float Pm; /* probability of mutation */
int n_mutation; /* number of mutations */
int n_cross; /* number of crossovers */
int print_arg; /* boolean for print or not to print population */
int bits_chrom; /* total bits in a chromosome */
unsigned *binary; /* binario que representa a codigo gray */    


// function variables
int a[2][25] = {{-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32},
                    {-32,-32,-32,-32,-32,-16,-16,-16,-16,-16,0,0,0,0,0,16,16,16,16,16,32,32,32,32,32}};

// random variables
double oldrand[55]; /* Array of 55 random numbers */
int jrand; /* current random number */
double rndx2; /* used with random normal deviate */
int rndcalcflag; /* used with random normal deviate */

