// merge procedure of merge sort
void merge(struct individual *pop, int start, int mid, int end){
	int n1, n2, i;	
	
	for(n1 = start, n2 = mid+1, i=start; n1 <= mid && n2 <= end; i++){
		if(pop[n1].fitness > pop[n2].fitness)
            temp_pop[i] = pop[n1++];
        else
            temp_pop[i] = pop[n2++];	
	}
   
    while(n1 <= mid)    
        temp_pop[i++] = pop[n1++];

    while(n2 <= end)   
        temp_pop[i++] = pop[n2++];

    for(i = start; i <= end; i++)
        pop[i] = temp_pop[i];
}

// the merge sort
void merge_sort(struct individual *pop, int start, int end){	
	if (start < end){
		int mid = (start + end) / 2;
		merge_sort(pop, start, mid);		
		merge_sort(pop, mid+1, end);
		merge(pop, start, mid, end);
	}else{
		return;
	}
}
