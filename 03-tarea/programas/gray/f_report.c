
/* Print the initial report of the parameters given by the user */
void initial_report() {
    printf("\nParameters used with the Genetic Algorithm \n");
    printf(" Chromosome length              =   %d\n", bits_chrom);
    printf(" Maximum number of generations  =   %d\n", Gmax);
    printf(" Total population size          =   %d\n", pop_size);        
    printf(" Crossover probability          =   %3.2f\n", Pc);
    printf(" Mutation probability           =   %3.2f\n\n", Pm);    
}

/* Write the population report on the screen */
void generation_report(){
    if(print_arg >= 3){
        printf("___________________________________________________________");
        printf("___________________________________________________________");
        printf("______________________________________________________________\n");
        printf("\t\t\t\t\t\t\t\tP O P U L A T I O N          R E P O R T  \n");
        printf(" Generation # %3d \t\t\t\t\t\t\t\t\t Generation # %3d\n", gen-1, gen);
        printf(" num\tx1          x2     fitness     %c_total  parents  ", '%');
        printf(" cross_p    x1          x2     fitness  %c_total\n", '%');
        printf("___________________________________________________________");
        printf("___________________________________________________________");
        printf("______________________________________________________________\n\n");

        write_pop();
    }

    /* write the summary statistics in global mode  */    
    if(print_arg >= 1){
        printf("___________________________________________________________");
        printf("___________________________________________________________");
        printf("______________________________________________________________\n");
        printf("Resume Generation # %d\n", gen);
        if(print_arg >= 2){
            printf("  Accumulated Statistics:\n");          
            printf("    Total Crossovers = %d, Total Mutations = %d\n", n_cross, n_mutation);
            printf("    min = %13.10lf   max = %13.10lf   avg = %13.10lf   sum = %lf\n", old_pop[pop_size-1].fitness, old_pop[0].fitness, avg, valesp_sum);
        }
        printf("  Best individual in this generation\n"); 
        write_best(best_ind_gen); printf("\n");       
    }else{
        if(gen%100 == 0)
            printf("Generation # %d\n", gen);
    }
}

void best_ever_report(){    
    printf("\n|---------------------------------------------");
    printf("-----------------------------------------------|\n");    
    printf("   Global best Individual so far, Generation # %d: \n", best_ever.generation);
    write_best( best_ever );     
    printf("|----------------------------------------------");
    printf("----------------------------------------------|\n\n"); 

}

/* Give the appropriate format to the several values that get printed */
void write_pop() {
    struct individual *pind;
    int j;
    int type_of_print = 3; 

    for (j = 0; j < pop_size; j++) {
        if(print_arg == 4){
            printf("    ");

            /* Old string */
            pind = & (old_pop[j]);                
            write_line(print_arg, pind); printf("\t"); 
            /* New string */
            pind = &(new_pop[j]);
            write_line(print_arg, pind); 
            printf("\n"); 
        }

        printf("%3d) ", j);

        /* Old string */
        pind = & (old_pop[j]);            
        write_line(type_of_print, pind);

        /* New string */
        pind = &(new_pop[j]);            
        printf("(%3d,%3d)  %3d  ", pind->parent[0], pind->parent[1], pind->cross_point);
        write_line(type_of_print, pind);        
          
        printf("\n");
    }
}

/* Print a line of the table, print the values of each chomosome */
void write_line(int type_of_print, struct individual *pind){
    switch(type_of_print){
        case 1: 
            write_chrom(pind->chrom);
            printf(" %12.10lf %3.3lf | ", pind->fitness, pind->val_esp);
            break;        
        case 2: 
            printf(" %9.5lf %9.5lf %10.6lf %4.4lf | ", pind->x1, pind->x2, pind->fitness, pind->val_esp);
            break;        
        case 3: 
            printf(" %14.10lf %14.10lf %10.6lf %4.4lf | ", pind->x1, pind->x2, pind->fitness, pind->val_esp);
            break;
        case 4: 
            write_chrom(pind->chrom);
            break;
    }
}

/* Print out each chromosome from the most significant bit to the least significant bit */

void write_chrom(unsigned *chrom){	
	for (int j = 0; j<bits_chrom; j++) {
		if (chrom[j]==0) printf("0");
			else printf("1");
	}
}

void write_best(struct best_ind ind){    
    printf("    ");     
    //for ( int j = (bits_chrom - 1); j >= 0; j--) {
    for (int j = 0; j<bits_chrom; j++) {
        if (ind.chrom[j] == 0) printf("0");
        else printf("1");
    }
    printf("\n    Fittest = %12.10lf   var = %9.5lf %9.5lf  \n", ind.fitness, ind.x1, ind.x2);    
}

/* Use a system call to clear the screen */
void cls(){
    system("clear");
}
