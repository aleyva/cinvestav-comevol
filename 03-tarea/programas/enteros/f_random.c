
/* Create next batch of 55 random numbers */
void advance_random(){
    int j;
    double new_random;

    for (j = 0; j < 24; j++) {
        new_random = oldrand[j] - oldrand[j + 31];
        if (new_random < 0.0) new_random = new_random + 1.0;
        oldrand[j] = new_random;
    }

    for (j = 24; j < 55; j++) {
        new_random = oldrand[j] - oldrand[j - 24];
        if (new_random < 0.0) new_random = new_random + 1.0;
        oldrand[j] = new_random;
    }
}

/* Flip a biased coin - true if heads */
int flip(float prob){
    float random_perc();

    if (random_perc() <= prob)
        return (1);
    else
        return (0);
}

/* initialization routine for random_normal_deviate */
void init_random_normal_deviate(){
    rndcalcflag = 1;
}

/* normal noise with specified mean & std dev: mu & sigma */

double noise(double mu, double sigma){
    double random_normal_deviate();
    return ((random_normal_deviate() *sigma) + mu);
}

/* Initialize random numbers batch */
void randomize() {
    for (int j = 0; j <= 54; j++)
        oldrand[j] = 0.0;
    jrand = 0;

    //random seed
    srand((unsigned)time(NULL));
    double seed = (double) rand() / (RAND_MAX);
    warmup_random(seed);
}

/* random normal deviate after ACM algorithm 267 / Box-Muller Method */
double random_normal_deviate() {
    double sqrt(), log(), sin(), cos();
    float random_perc();
    double t, rndx1;

    if (rndcalcflag) {
        rndx1 = sqrt(-2.0 *log((double) random_perc()));
        t = 6.2831853072 *(double) random_perc();
        rndx2 = sin(t);
        rndcalcflag = 0;
        return (rndx1 *cos(t));
    } else {
        rndcalcflag = 1;
        return (rndx2);
    }
}

/* Fetch a single random number between 0.0 and 1.0 - Subtractive Method */
/* See Knuth, D. (1969), v. 2 for details */
/* name changed from random() to avoid library conflicts on some machines*/
float random_perc(){
    jrand++;

    if (jrand >= 55) {
        jrand = 1;
        advance_random();
    }

    return ((float) oldrand[jrand]);
}

/* Pick a random integer between low and high */
int rnd_int(int low, int high){
    int i;
    float random_perc();

    if (low >= high)
        i = low;
    else{
        i = (random_perc() *(high - low + 1)) + low;
        if (i > high) 
            i = high;
    }
    return (i);
}

/* real random number between specified limits */

float rnd_real(float lo, float hi){
    return ((random_perc() *(hi - lo)) + lo);
}

/* Get random off and running */
void warmup_random(float random_seed){
    int ii;
    double new_random, prev_random;

    oldrand[54] = random_seed;
    new_random = 0.000000001;
    prev_random = random_seed;

    for (int j = 1; j <= 54; j++){
        ii = (21 *j) % 54;
        oldrand[ii] = new_random;
        new_random = prev_random - new_random;
        if (new_random < 0.0) new_random = new_random + 1.0;
        prev_random = oldrand[ii];
    }

    advance_random();
    advance_random();
    advance_random();

    jrand = 0;
}
