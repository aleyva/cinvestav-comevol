/*============================================================================= 
 |
 |       Author:  Antonio Leyva
 |     Language:  C
 |
 |   To Compile:  gcc genetic.c -o genetic.out -lm
 |       To Run:  
 					./genetic.out 150 300 .8 .1 0
					for help, execute
                    ./genetic.out

 |        Class:  Computación Evolutiva
 |   Instructor:  Carlos Coello
 |     Due Date:  21-jun-2019
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Programa algoritmo genético simple para minimizar la funcion  |                
 				1/K + Sum j=1 to 25 (fj⁻¹(x1,x2))
                donde fj(x1,x2) = cj + sum i=1 to 2 (xi−aij)⁶
                − 65.536 ≤ x i ≤ 65.536
                K = 500
                cj = j 
                y: [−32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32] 
                   [−32 −32 −32 −32 −32 −16 −16 −16 −16 −16   0   0   0   0   0  16  16  16  16  16  32  32  32  32  32];
                   optimal values -32 -32
            Con:  representacion entera
                  cruza de un punto, 
                  mutación uniforme 
                  selección mediante jerarquı́as lineales (usar Max=1.1) a la selección universal estocástica
 |
 |        Input:  mutation rate, crossover probability, number of generations
 |        Output: 
 |
 *===========================================================================*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "genetic.h"
#include "f_random.c"
#include "f_report.c"
#include "f_memory.c"
#include "f_merge-sort.c"

int main(int argc, char **argv) {
    cls();
    
    if(argc != 6){
        printf("\nERROR\nParámetros incorrectos: <Gmax> <Pop_size> <Pc> <Pm> <print_arg>\n");
        printf(" - Gmax : maximum number of generations\n");
        printf(" - Pop_size : population size\n");        
        printf(" - Pc : probability of crossover, valor entre (0 1)\n");        
        printf(" - Pm : probability of mutation, valor entre (0 1)\n");        
        printf(" - print_arg : \n");        
        printf("\t4 for print all details; all generation and statistics v1\n");
        printf("\t3 for print all details; all generation and statistics v2\n");
        printf("\t2 for print statistics in each generation (omit the generation detail)\n");
        printf("\t1 for print only the best individual of each generation\n");        
        printf("\t0 for print only best ever individual\n");        
        exit(1);
    }

    Gmax = atoi(argv[1]); // reading the maximum number of generations        
    pop_size = atoi(argv[2]); /* Population size */     
    Pc = atof(argv[3]); // reading the probability of crossover     
    Pm = atof(argv[4]); // reading mutation rate 
    print_arg = atoi(argv[5]); // reading print report parameter */   

    if(pop_size%2 == 1)
            pop_size = pop_size+1;

    // the total bits of a chromosome, is the lenght of each variable * 2.
    // all the 2 variables are put together in the chromosome
    bits_chrom = BITSVAR * VAR;    

    /* Perform the previous memory allocation */
    memory_allocation();

    /* Initialize random number generator */
    randomize();

    // variables initialization
    n_mutation = 0;
    n_cross = 0;    

    /* Initialize the populations and report statistics */
    initialize_pop();
    statistics(old_pop);
    initial_report();

    struct individual *temp;    
    for (gen = 1; gen <= Gmax; gen++){ // Iterate the given number of generations  {
        // Create a new generation 
        create_generation();
        // Compute fitness statistics on new populations 
        statistics(new_pop);
        // Report results for new generation 
        generation_report();
        // Advance the generation 
        temp = old_pop;
        old_pop = new_pop;
        new_pop = temp;
    }

    //report the best individual
    best_ever_report();

    free_all();
    return 0;
}

/* Define randomly the population for generation # 0 */
void initialize_pop() {    
    int i, k;
    for (i = 0; i < pop_size; i++) {        
        for (k = 0; k < BITSVAR*VAR; k++) {
            old_pop[i].chrom[k] = rnd_int(0, 9)+48;            
        }         
        decode( &old_pop[i] );
        obj_func( &old_pop[i] );
        old_pop[i].parent[0] = 0; /* Initialize parent info */
        old_pop[i].parent[1] = 0;
        old_pop[i].cross_point = -1; /* Initialize crossover sites */                                                     
    }
}

/* Perform all the work involved with the creation of a new generation of chromosomes */
void create_generation() {
    /* select, crossover, and mutation */
    n_mutation = 0;
    n_cross = 0;
    int j = 0;    

    /* Selection */
    selection();

    for(int j = 0; j < pop_size - 1; j+=2){    
        
        /* Crossover */
        crossover( j, j+1 );

        /* mutation */
        mutation( &new_pop[j] );
        mutation( &new_pop[j+1] );

        /* Decode string, evaluate fitness, & record */
        /* parentage date on both children */
        decode( &new_pop[j] );
        decode( &new_pop[j+1] );    
        obj_func( &new_pop[j] );
        obj_func( &new_pop[j+1] );
        
    }

    //Elitism, replace the best individual of the last generation into the new one
    copy_bits(new_pop[0].chrom, best_ind_gen.chrom, 0, bits_chrom);       
    obj_func( &new_pop[0] );
}


/* Calculate population statistics */
void statistics(struct individual *pop) {
    
    valesp_sum = 0.0;        
    double fitness_sum = 0.0;
    int last_ind = pop_size-1;
    
    // order values considering the fitness
    merge_sort(pop, 0, pop_size-1);  

    for (int j = 0; j < pop_size; j++) {
        //min + (max - min) (jerarquia-1 / pop_size-1)    
        // max = 1.1 y min = 0.9;
        pop[j].val_esp = 0.9 + (0.2 * ((double)j / last_ind) );                
        valesp_sum += pop[j].val_esp;
        fitness_sum += pop[j].fitness;
    } 

    // set the fittest in generation    
    best_ind_gen.chrom = pop[last_ind].chrom;
    best_ind_gen.fitness = pop[last_ind].fitness;
    best_ind_gen.x1 = pop[last_ind].x1;
    best_ind_gen.x2 = pop[last_ind].x2;
    
    /* Define new global best_ever-fit individual */
    if (pop[last_ind].fitness < best_ever.fitness || gen == 1) {
        copy_bits(best_ever.chrom, pop[last_ind].chrom, 0, bits_chrom);
        best_ever.fitness = pop[last_ind].fitness;
        best_ever.x1 = pop[last_ind].x1;
        best_ever.x2 = pop[last_ind].x2;
        best_ever.generation = gen;
    } 
    
    /* Calculate average */
    avg = fitness_sum / pop_size;
}

/* Implementation of a roulette selection process */
int selection() {    
    int i, j;        
    double rand1, sum;       

    int idx_new_pop = 0;

    /* regresa un número aleatorio entre 0 y 1 */    
    rand1 = random_perc();
    for(sum=0,i=0; i < pop_size; i++){
        for(sum += old_pop[i].val_esp; sum > rand1; rand1++, idx_new_pop++){
            //copy the selected individual            
            copy_bits(new_pop[idx_new_pop].chrom, old_pop[i].chrom, 0, bits_chrom);            
        }
    }
}

/* Cross 2 parent strings, place in 2 child strings */
void crossover( int idx_parent1, int idx_parent2){
    
    /* Do crossover with probability Pc */
    if (flip(Pc)) {
        n_cross++;
        
        struct individual *child1 = &new_pop[idx_parent1];
        struct individual *child2 = &new_pop[idx_parent2];

        // rescatar a los padres por que se van a sustituir por los hijos
        copy_bits( temp_child1_chrom, new_pop[idx_parent1].chrom, 0, bits_chrom );
        copy_bits( temp_child2_chrom, new_pop[idx_parent2].chrom, 0, bits_chrom );        
        
       /* Define the crosspoint between 1 and length-2 */ 
        int cross_point = rnd_int(2, (bits_chrom - 2));
        //cross_point = rnd_int(1, 3) * BITSVAR; /* Crosspoint 1 between 1 and length-1 */        
        
        // do the crossover
        copy_bits( child1->chrom, temp_child1_chrom, 0, cross_point );
        copy_bits( child1->chrom, temp_child2_chrom, cross_point, bits_chrom );        
        copy_bits( child2->chrom, temp_child2_chrom, 0, cross_point );
        copy_bits( child2->chrom, temp_child1_chrom, cross_point, bits_chrom );
        
        child1->parent[0] = child2->parent[0] = idx_parent1;
        child1->parent[1] = child2->parent[1] = idx_parent2; 
        child1->cross_point = child2->cross_point = cross_point;       
        
    } else {          
        // copy exactly, parents to children        
        new_pop[idx_parent1].parent[0] = new_pop[idx_parent2].parent[0] = -1;
        new_pop[idx_parent1].parent[1] = new_pop[idx_parent2].parent[1] = -1;    
        new_pop[idx_parent1].cross_point = new_pop[idx_parent2].cross_point = -1; 
    }          
}

/* Perform a mutation in a random string, and keep track of it */
void mutation(struct individual *ind){
    int k;
    
    /* Do mutation with probability Pm */
    for (k = 0; k < bits_chrom; k++) {
        if (flip(Pm)){
            n_mutation++;            
            ind->chrom[k] = rnd_int(0, 9)+48;
            
        }
    } 
}

/* Copy a range of allel, from source to destiny */
void copy_bits(unsigned *dest, unsigned *source, int begin, int end){
    for (int i = begin; i < end; i++){
        dest[i] = source[i];
    }    
}

/* Decode the decimal representation of the chromosome to a real number 
in the range and store in the variables of the individual */
int decode(struct individual *ind){
    int i, j;             
    long num;       
    double num_d;
    char buffer[BITSVAR];
    long max_range = pow(10,BITSVAR)-1;

    // para cada variable, convertir de entero -> rango -> decimal -> real 
    for (i = 0; i < VAR; i++) {        
        // adjust range
        for (j = 0; j < BITSVAR; j++)        
            buffer[j] = ind->chrom[j];

        num = atol(buffer);
        num_d = -65.536 + ( ((double)num * 131.072) / max_range);            
        switch (i){
            case 0: ind->x1 = num_d; break;
            case 1: ind->x2 = num_d; break;            
        }        
    }   
}

/* Define the objective function dejong5. In this case */
//                1
// ----------------------------------
// 1/K + Sum j=1 to 25 (fj⁻¹(x1,x2))
// donde fj(x1,x2) = cj + sum i=1 to 2 (xi−aij)⁶
// − 65.536 ≤ x i ≤ 65.536
// K = 500
// cj = j 
// y: [−32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32] 
//    [−32 −32 −32 −32 −32 −16 −16 −16 −16 −16   0   0   0   0   0  16  16  16  16  16  32  32  32  32  32];
void obj_func(struct individual *ind){    
    double term1, term2, sum;
    int a1, a2, i;
    sum = 0;

    for (i = 0; i<25; i++){
        a1 = a[0][i];
        a2 = a[1][i];
        term1 = pow( (ind->x1 - a1), 6 );
        term2 = pow( (ind->x2 - a2), 6 );        
        sum += 1 / (i+term1+term2);
    }
    ind->fitness = 1 / (0.002 + sum);    
}



