#include<stdio.h>
#include<stdlib.h>
#include<math.h>

//                1
// ----------------------------------
// 1/K + Sum j=1 to 25 (fj⁻¹(x1,x2))

// donde fj(x1,x2) = cj + sum i=1 to 2 (xi−aij)⁶
// − 65.536 ≤ x i ≤ 65.536
// K = 500
// cj = j 
// y: [−32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32 −32 −16   0  16  32] 
//    [−32 −32 −32 −32 −32 −16 −16 −16 −16 −16   0   0   0   0   0  16  16  16  16  16  32  32  32  32  32];


int a[2][25] = {{-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32},
                    {-32,-32,-32,-32,-32,-16,-16,-16,-16,-16,0,0,0,0,0,16,16,16,16,16,32,32,32,32,32}};

double dejongf5(double x1, double x2){
    double term1, term2, sum;
    int a1, a2, i;
    sum = 0;

    for (i = 0; i<25; i++){
        a1 = a[0][i];
        a2 = a[1][i];
        term1 = pow( (x1 - a1), 6 );
        term2 = pow( (x2 - a2), 6 );        
        sum += 1 / (i+term1+term2);
    }
    return 1 / (0.002 + sum);
}

int main(int argc, char *argv[] ){
    printf("%lf\n", dejongf5(atof(argv[1]), atof(argv[2])));
}
