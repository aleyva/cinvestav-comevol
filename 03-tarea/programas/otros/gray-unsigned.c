// C++ program for Binary To Gray 
// and Gray to Binary conversion 
#include <iostream> 
using namespace std; 
  
int bits_chrom = 5;

// Helper function to xor two characters 
char xor_c(char a, char b) { return (a == b) ? '0' : '1'; } 
  
// Helper function to flip the bit 
char flip(char c) { return (c == 0) ? 1 : 0; } 
  
// function to convert binary string 
// to gray string 
string binarytoGray(string binary) 
{ 
    string gray = ""; 
  
    // MSB of gray code is same as binary code 
    gray += binary[0]; 
  
    // Compute remaining bits, next bit is comuted by 
    // doing XOR of previous and current in Binary 
    for (int i = 1; i < binary.length(); i++) { 
        // Concatenate XOR of previous bit 
        // with current bit 
        gray += xor_c(binary[i - 1], binary[i]); 
    } 
  
    return gray; 
} 
  
// function to convert gray code string 
// to binary string 
unsigned * graytoBinary(unsigned *gray) 
{     
    unsigned *binary;    
    unsigned numbytes = bits_chrom * sizeof(unsigned *);
    
    // if ((binary = (unsigned *) malloc(numbytes)) == NULL)
        // no_memory("old population chromosomes");
    binary = (unsigned *) malloc(numbytes);


    // MSB of binary code is same as gray code 
    binary[bits_chrom-1] = gray[bits_chrom-1]; 
  
    // Compute remaining bits 
    for (int i = bits_chrom-2; i>=0; i--) { 
        // If current bit is 0, concatenate 
        // previous bit 
        if (gray[i] == 0) 
            binary[i] = binary[i + 1]; 
  
        // Else, concatenate invert of 
        // previous bit 
        else
            binary[i] = flip(binary[i + 1]); 
    } 
  
    return binary; 
} 

void write_chrom(unsigned *chrom){	
	for (int j = 0; j<bits_chrom; j++) {
		if (chrom[j]==0) printf("0");
			else printf("1");
	}
}
  
// Driver program to test above functions 
int main() 
{ 
    unsigned *ind1, *ind2;    
    unsigned numbytes = bits_chrom * sizeof(unsigned *);
    
    // if ((ind1 = (unsigned *) malloc(numbytes)) == NULL)
        // no_memory("old population chromosomes");

    // if ((ind2 = (unsigned *) malloc(numbytes)) == NULL)
        // no_memory("new population chromosomes");

    string binary = "01101"; 
    ind1 = (unsigned *) malloc(numbytes);
    for (int j = 0; j<bits_chrom; j++) {
		ind1[0] = 0;
	}
    ind1[0] = 1;
    ind1[2] = 1;    
    ind1[3] = 1;

    // ind2[0] = 1;
    // ind2[1] = 0;
    // ind2[2] = 0;
    // ind2[3] = 1;
    // ind2[4] = 0;

    write_chrom(ind1);
    cout << endl;
    cout << "Gray code of " << binary << " is "; 
    write_chrom(graytoBinary(ind1));
    cout << endl;
    cout << "could be 01001" << endl; 
    // string gray = "01101"; 
    // cout << "Binary code of " << gray << " is "
    //      << graytoBinary(gray) << endl; 
    return 0; 
} 