/* Global structures and variables */

struct individual {
    unsigned *chrom;             /*      chromosome string for the individual */
    double w;			         /*	          value of the decoded string */
    double x;			         /*	          value of the decoded string */
    double y;			         /*	          value of the decoded string */
    double z;			         /*	          value of the decoded string */
    double fitness;              /* 	            fitness of the individual */
    double percent;
    int    cross_point;          /* 	           crossover site 1 at mating */                    
    int    parent[2];            /*         who the parents of offspring were */
};

struct best_ind{
    unsigned *chrom;        /* chromosome string for the best_ind-ever individual */
    double w;			    /*	          value of the decoded string */
    double x;			    /*	          value of the decoded string */
    double y;			    /*	          value of the decoded string */
    double z;			    /*	          value of the decoded string */
    double   fitness;       /*            fitness of the best_ind-ever individual */
    int      generation;    /*                   generation which produced it */
};

/* Functions prototypes */

void memory_allocation();
void no_memory(char *);
void free_all();

void initialize_pop();
void create_generation();
void statistics(struct individual *);
int selection();
void mutation(struct individual *);
void crossover(int, int);
void obj_func(struct individual *);
int flip(float);
void copy_bits(unsigned *, unsigned *, int, int);
int decode(struct individual *);

// report and console functions
void cls();
void initial_report();
void generation_report();
void best_ever_report();
void write_pop();
void write_line(int, struct individual *);
void write_chrom(unsigned *);
void write_best(struct best_ind ind);

// random functions
void warmup_random(float);
float rnd_real(float,float);
int rnd_int(int,int);
float random_perc();
double random_normal_deviate();
void randomize();
double noise(double,double);
void init_random_normal_deviate();
void advance_random();


// variables

struct individual *old_pop; /* last generation of individuals */
struct individual *new_pop; /* next generation of individuals */
struct best_ind best_ind_gen; /* fittest individual per generation */
struct best_ind best_ever; /* fittest individual so far */
double fitness_sum; /* summed fitness for entire population */
double fitness_max; /* maximum fitness of population */
double fitness_min; /* minimum fitness of population */
double avg; /* average fitness of population */
int gen; /* current generation number */
int Gmax; /* maximum number of generations */
int pop_size; /* population size */
float Pc; /* probability of crossover */
float Pm; /* probability of mutation */
int n_mutation; /* number of mutations */
int n_cross; /* number of crossovers */
int print_arg; /* boolean for print or not to print population */
int bits_chrom; /* total bits in a chromosome */

// random variables
double oldrand[55]; /* Array of 55 random numbers */
int jrand; /* current random number */
double rndx2; /* used with random normal deviate */
int rndcalcflag; /* used with random normal deviate */

