#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main(int argc, char **argv){
    if(argc != 5)
        exit(1);

    float w, x, y, z;
    double f1, f2, f3, f4, f5, f6;    
    
    //double var[4] = {1.40625, 1.97754, 0.14160, -0.00244};
    w = atof(argv[0]);      
    x = atof(argv[1]);
    y = atof(argv[2]); 
    z = atof(argv[3]); 
    
    //seed(time(0));
    f1 = pow(10*(x-pow(w,2)),2);
    f2 = pow(1-w,2);
    f3 = 90 * pow(z-pow(y,2),2);
    f4 = pow(1-y,2); 
    f5 = 10 * pow(x+z-2,2); 
    f6 = 0.1 * pow(x-z,2);
    
    printf("%lf\n", ( f1 + f2 + f3 + f4 + f5 + f6 ));

}

