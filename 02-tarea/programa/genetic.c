/*============================================================================= 
 |
 |       Author:  Antonio Leyva
 |     Language:  C
 |
 |   To Compile:  gcc genetic.c -o genetic.out -lm
 |       To Run:  
 					./genetic.out 300 5000 .9 .03 2
					for help, execute
                    ./genetic.out

 |        Class:  Computación Evolutiva
 |   Instructor:  Carlos Coello
 |     Due Date:  04-jun-2019
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Programa algoritmo genético simple para minimizar la funcion  |                
 				f(x1,x2,x3,x4)=[10(x2-x1²)]²+(1-x1)²
				 				+90(x4-x3²)²+(1-x3)²+
								+10(x2+x4-2)²+0.1(x2-x4)²
            Con:  cruza de un punto, 
                  mutación uniforme 
                  selección proporcional de ruleta
 |
 |        Input:  mutation rate, crossover probability, number of generations
 |        Output: 
 |
 *===========================================================================*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "genetic.h"
#include "f_random.c"
#include "f_report.c"
#include "f_memory.c"

#define BITSVAR 20 // Number of bits used per variable of the function. 
                    //In this problems, there are 4 variables

int main(int argc, char **argv) {
    //cls();
    
    if(argc != 6){
        printf("\nERROR\nParámetros incorrectos: <Gmax> <Pop_size> <Pc> <Pm> <print_arg>\n");
        printf(" - Gmax : maximum number of generations\n");
        printf(" - Pop_size : population size\n");        
        printf(" - Pc : probability of crossover\n");        
        printf(" - Pm : probability of mutation\n");        
        printf(" - print_arg : \n");        
        printf("\t4 for print all details; all generation and statistics v1\n");
        printf("\t3 for print all details; all generation and statistics v2\n");
        printf("\t2 for print statistics in each generation (omit the generation detail)\n");
        printf("\t1 for print only the best individual of each generation\n");        
        printf("\t0 for print only best ever individual\n");
        //std::cout << "El número de parametros es incorrecto. Ejecuta './" << argv[0] << "-h' para abrir la ayuda" << std::endl;
        exit(1);
    }

    Gmax = atoi(argv[1]); // reading the maximum number of generations        
    pop_size = atoi(argv[2]); /* Population size */     
    Pc = atof(argv[3]); // reading the probability of crossover     
    Pm = atof(argv[4]); // reading mutation rate 
    print_arg = atoi(argv[5]); // reading print report parameter */   

    // the total bits of a chromosome, is the lenght of each variable * 4.
    // all the 4 variables are put together in the chromosome
    bits_chrom = BITSVAR * 4;    

    /* Perform the previous memory allocation */
    memory_allocation();

    /* Initialize random number generator */
    randomize();

    // variables initialization
    n_mutation = 0;
    n_cross = 0;
    best_ever.fitness = 0;
    best_ever.generation = 0;

    /* Initialize the populations and report statistics */
    initialize_pop();
    statistics(old_pop);
    initial_report();

    struct individual *temp;    
    for (gen = 1; gen <= Gmax; gen++){ // Iterate the given number of generations  {
        // Create a new generation 
        create_generation();
        // Compute fitness statistics on new populations 
        statistics(new_pop);
        // Report results for new generation 
        generation_report();
        // Advance the generation 
        temp = old_pop;
        old_pop = new_pop;
        new_pop = temp;
        
        // falta liberar la informacion de cada poblacion        
    }

    //report the best individual
    best_ever_report();

    free_all();
    return 0;
}

/* Define randomly the population for generation # 0 */
void initialize_pop() {    
    int i, k;
    for (i = 0; i < pop_size; i++) {        
        for (k = 0; k < BITSVAR*4; k++) {
            old_pop[i].chrom[k] = flip(0.5);            
        }        
        decode( &old_pop[i] );
        obj_func( &old_pop[i] );
        old_pop[i].parent[0] = 0; /* Initialize parent info */
        old_pop[i].parent[1] = 0;
        old_pop[i].cross_point = -1; /* Initialize crossover sites */                                                     
    }
}

/* Perform all the work involved with the creation of a new generation of chromosomes */
void create_generation() {
    /* select, crossover, and mutation */
    n_mutation = 0;
    n_cross = 0;
    int j = 0;    
    for(int j = 0; j < pop_size - 1; j+=2){    
        /* Crossover */
        crossover( j, j+1 );

        /* mutation */
        mutation( &new_pop[j] );
        mutation( &new_pop[j+1] );

        /* Decode string, evaluate fitness, & record */
        /* parentage date on both children */
        decode( &new_pop[j] );
        decode( &new_pop[j+1] );    
        obj_func( &new_pop[j] );
        obj_func( &new_pop[j+1] );
                
        /* Increment population index */
    }

    //Elitism, replace the best individual of the last generation into the new one
    copy_bits(new_pop[0].chrom, best_ind_gen.chrom, 0, bits_chrom);
    decode( &new_pop[0] );            
    obj_func( &new_pop[0] );
}


/* Calculate population statistics */
void statistics(struct individual *pop) {
    
    fitness_sum = 0.0;
    fitness_min = pop[0].fitness;
    fitness_max = pop[0].fitness;
    best_ind_gen.chrom = pop[0].chrom;
    best_ind_gen.fitness = pop[0].fitness;

    /* Loop for max, min */
    for (int j = 0; j < pop_size; j++) {
        fitness_sum = fitness_sum + pop[j].fitness; /* Accumulate */        
        
        if (pop[j].fitness < fitness_min){
            fitness_min = pop[j].fitness; /* New minimum */            
        }
        if (pop[j].fitness > fitness_max) {
            fitness_max = pop[j].fitness; /* New maximum */                                   
            best_ind_gen.chrom = pop[j].chrom;
            best_ind_gen.fitness = pop[j].fitness;
            best_ind_gen.w = pop[j].w;
            best_ind_gen.x = pop[j].x;
            best_ind_gen.y = pop[j].y;
            best_ind_gen.z = pop[j].z;

            /* Define new global best_ever-fit individual */
            if (pop[j].fitness > best_ever.fitness) {                
                copy_bits(best_ever.chrom, pop[j].chrom, 0, bits_chrom);
                best_ever.fitness = pop[j].fitness;
                best_ever.w = pop[j].w;
                best_ever.x = pop[j].x;
                best_ever.y = pop[j].y;
                best_ever.z = pop[j].z;
                best_ever.generation = gen;
            } 
        }
    }
    
    /* Loop for computate each chomosome percent (for rulette purpose) */               
    for (int j = 0; j < pop_size; j++) {
        pop[j].percent = pop[j].fitness / fitness_sum;
    }

    /* Calculate average */
    avg = fitness_sum / pop_size;
}

/* Implementation of a roulette selection process */
int selection() {    
    int j;        
    double rand1, fit_roulette;

    //rand1 = rnd_real(0, 1);    
    rand1 = random_perc();
    fit_roulette = 0.0;    

    /* Loop for max, min, fitness_sum */
    for (j = 0; j < pop_size; j++){
        fit_roulette = fit_roulette + old_pop[j].percent; /* Accumulate */  
        if(rand1 < fit_roulette)
            break;
    }
    return (j);

}

/* Cross 2 parent strings, place in 2 child strings */
void crossover( int pop_ind1_index, int pop_ind2_index){
    
    int j;
    struct individual *child1 = &new_pop[pop_ind1_index];
    struct individual *child2 = &new_pop[pop_ind2_index];
    
    /* Do crossover with probability Pc */
    if (flip(Pc)) {
        n_cross++;

        struct individual *parent1, *parent2;
        int parent1_index, parent2_index, cross_point;

        /* pick a pair of mates */
        parent1_index = selection();
        parent2_index = selection();
        // do{
        //     parent2_index = selection();
        // }while(parent1_index == parent2_index);        
        parent1 = &old_pop[parent1_index];
        parent2 = &old_pop[parent2_index];

        /* Define the crosspoint between 1 and length-2 */ 
        //cross_point = rnd_int(2, (bits_chrom - 2));
        cross_point = rnd_int(1, 3) * BITSVAR; /* Crosspoint 1 between 1 and length-1 */        
        
        // do the crossover
        copy_bits( child1->chrom, parent1->chrom, 0, cross_point );
        copy_bits( child1->chrom, parent2->chrom, cross_point, bits_chrom );        
        copy_bits( child2->chrom, parent2->chrom, 0, cross_point );
        copy_bits( child2->chrom, parent1->chrom, cross_point, bits_chrom );
        
        child1->parent[0] = child2->parent[0] = parent1_index;
        child1->parent[1] = child2->parent[1] = parent2_index; 
        child1->cross_point = child2->cross_point = cross_point;       
        
    } else {          
        // copy exactly, parents to children
        copy_bits( child1->chrom, old_pop[pop_ind1_index].chrom, 0, bits_chrom );
        copy_bits( child2->chrom, old_pop[pop_ind2_index].chrom, 0, bits_chrom );
        child1->parent[0] = child2->parent[0] = -1;
        child1->parent[1] = child2->parent[1] = -1;    
        child1->cross_point = child2->cross_point = -1; 
    }          
}

/* Perform a mutation in a random string, and keep track of it */
void mutation(struct individual *ind){
    int place_mut, k;

    /* Do mutation with probability Pm */
    for (k = 0; k < bits_chrom; k++) {
        if (flip(Pm)){
            n_mutation++;
            if ((ind->chrom[k]) == 0) 
                ind->chrom[k] = 1;
            else 
                ind->chrom[k] = 0;                
        }
    } 
}

/* Copy a range of allel, from source to destiny */
void copy_bits(unsigned *dest, unsigned *source, int begin, int end){
    for (int i = begin; i < end; i++){
        dest[i] = source[i];
    }    
}

int decode(struct individual *ind){
    int i, j;    
    double accum;      

    for (i = 0; i < 4; i++) {
        accum = 0;
        for (j = 0; j < BITSVAR; j++) {           
            if (ind->chrom[j + (BITSVAR * i) ] == 1){ 
                accum += pow(2.0, (double) j);
            }
        }    
        
        /* x = linf + x' * (lsup-linf)/(2^L - 1)
        x' : es el entero largo que obtienes de la decodificación (o sea, de sumar las potencias de 2 de la cadena binaria). 
        lsup y linf:  son el límite superior e inferior de la variable, respectivamente (20 y -20 en este caso). 
        L : es la longitud de la cadena correspondiente a una variable (BITSVAR bits en este caso).
        */
        
        //x = -20 + acum * (20-(-20)) / (pow(2,BITSVAR) - 1)
        //x = -20 + (acum * 40) / ( pow(2,BITSVAR) - 1 )
        accum = -20 + (accum * 40) / ( pow(2,BITSVAR) - 1 );      
        switch (i){
            case 0: 
                ind->w = accum; break;
            case 1:
                ind->x = accum; break;
            case 2:
                ind->y = accum; break;
            case 3:
                ind->z = accum; break;
        }
    }    
}

/* Define the objective function. In this case, 
f(w,x,y,z) = (10*(x-w²))² + (1-w)²  + 90*(z-y²)² + (1-y)² + 10*(x+z-2)² + 0.1*(x-z)²
*/
void obj_func(struct individual *ind){
    double w, x, y, z;
    double f1, f2, f3, f4, f5, f6;    
    w = ind->w;
    x = ind->x;
    y = ind->y;
    z = ind->z;
    
    //f(w,x,y,z) = (10*(x-w²))² + (1-w)²  + 90*(z-y²)² + (1-y)² + 10*(x+z-2)² + 0.1*(x-z)²
    f1 = pow(10*(x-pow(w,2)),2);
    f2 = pow(1-w,2);
    f3 = 90 * pow(z-pow(y,2),2);
    f4 = pow(1-y,2); 
    f5 = 10 * pow(x+z-2,2); 
    f6 = 0.1 * pow(x-z,2);
    // 1 / value of function to get the inverse of fitness
    //ind->fitness = 1 / abs(f1 + f2 + f3 + f4 + f5 + f6);             
    ind->fitness = 1 / (f1 + f2 + f3 + f4 + f5 + f6);         
}
