
#define VAR 20      // number of variables


/* Global structures and variables */

struct individual {
    double *chrom;             /*      chromosome string for the individual */    
    double fitness;            /* fitness of the individual in this case the evaluation costo of the individual */        
    double f_x;                /* the value of objective function */        
    double penalty;
    int    parent[2];            /*         who the parents of offspring were */
};

struct best_ind{
    double *chrom;        /* chromosome string for the best_ind-ever individual */    
    double fitness;       /* fitness of the individual in this case the evaluation costo of the individual */            
    double f_x;           /* the value of objective function */        
    int    generation;    /*                       generation which produced it */
};

//------------------------
/* Functions prototypes */
//------------------------
void memory_allocation();
void no_memory(char *);
void free_all();

void initialize_pop();
void create_generation();
void statistics(struct individual *);
int selection();
void one_point_crossover(struct individual *,struct individual *,struct individual *,struct individual *);
void crossover(int, int, int, int);
void mutation(struct individual *);
void inversion(struct individual *);
void obj_func(struct individual *);
double get_penalty(double, double, double, double);

void copy_bits(double *, double *, int, int);
void shuffle();
void scan_float(char *, float *, float, float);

// report and console functions
void cls();
void initial_report();
void generation_report();
void best_ever_report();
void write_pop();
void write_line(int, struct individual *);
void write_chrom(double *);
void write_best(struct best_ind ind);

// random functions
void warmup_random(float);
float rnd_real(float,float);
int rnd_int(int,int);
float random_perc();
double random_normal_deviate();
void randomize();
double noise(double,double);
void init_random_normal_deviate();
void advance_random();
int flip(float);

//------------------------
//       variables
//------------------------

// control and program variables
int gen; /* current generation number */
double *temp; /* temporal vector for copy parent bits  */
struct individual *old_pop; /* last generation of individuals */
struct individual *new_pop; /* next generation of individuals */
static int *tourn_list, tourn_pos, tourn_size; /* Tournment list, position in list */

// statistics variables
struct best_ind best_ind_gen; /* fittest individual per generation */
struct best_ind best_ever; /* fittest individual so far */
double fitness_sum; /* summed fitness for entire population */
double fitness_max; /* maximum fitness of population */
double fitness_min; /* minimum fitness of population */
double avg; /* average fitness of population */
int n_mutation; /* number of mutations */
int n_cross; /* number of crossovers */

// arguments
int Gmax; /* maximum number of generations */
int pop_size; /* population size */
float Pc; /* probability of crossover */
float Pm; /* probability of mutation */
float Pen_f; /* Penalty factor */
int print_arg; /* boolean for print or not to print population */

// random variables
float r_seed;  /* Seed for generate random numbers */
double oldrand[55]; /* Array of 55 random numbers */
int jrand; /* current random number */
double rndx2; /* used with random normal deviate */
int rndcalcflag; /* used with random normal deviate */

// constants
double mutation_constant; /* constant value in a generation, part of the delta mutation */
double lb_ub1[2];
double penalty_vec[2];
