/*============================================================================= 
 |
 |       Author:  Antonio Leyva
 |     Language:  C
 |
 |   To Compile:  gcc genetic.c -o genetic.out -lm
 |
 |   To execute (no parameters):  					
                    ./genetic.out
 |   To execute (with parameters):  					 
 					./genetic.out 10000 1000 .4 .05 0
 
 |        Class:  Computación Evolutiva
 |   Instructor:  Carlos Coello
 |     Due Date:  16-jul-2019
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Programa algoritmo genético para resolver problemas con manejo de restricciones 				                                
                    v = 20
                    n = 2 (1 linear inequality, 1 nonlinear inequality; h1 almost active (−10−8))
                    nonlinear objective function
                    f(x) =  G2
                    // restrictions
                    h1(x) = 0.75 − Qvi=1toV xi ≤ 0
                    h2(x) = Σi=1toV xi − 7.5v ≤ 0
                    xi ∈ [0, 10], i = 1, . . . , v
                    best known value from [163
                    best known f(x∗) = −0.8036                    
                    
           With:  Selección: por torneo, tamaño de torneo = 2
                  Cruza: Un punto
                  Mutación: No uniforme
                  Restriccion: Penalizacion estática usando mejor solución                
                  

 |
 *===========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>   
#include "genetic.h"
#include "../lib/f_random.c"
#include "../lib/f_memory.c"
#include "f_report.c"

int main(int argc, char **argv) {
    cls();
    char *file;

    if(argc != 6 && argc != 1){
        printf("\nERROR\nParámetros incorrectos: <Gmax> <Pop_size> <Pc> <Pm> <Pen_value> <print_arg>\n");
        printf(" - Gmax : maximum number of generations\n");
        printf(" - Pop_size : population size\n");        
        printf(" - Pc : probability of crossover\n");        
        printf(" - Pm : probability of mutation\n");                                      
        printf(" - print_arg : \n");                
        printf("\t3 for print all details; all generation and statistics\n");
        printf("\t2 for print statistics in each generation (omit the generation detail)\n");
        printf("\t1 for print only the best individual of each generation\n");        
        printf("\t0 for print only best ever individual\n");
        exit(1);
    }

    if(argc == 6){
        Gmax = atoi(argv[1]); // reading the maximum number of generations        
        pop_size = atoi(argv[2]); /* Population size */     
        Pc = atof(argv[3]); // reading the probability of crossover     
        Pm = atof(argv[4]); // reading mutation rate                 
        print_arg = atoi(argv[5]); // reading print report parameter */                   
        //random seed
        srand((unsigned)time(NULL));
        r_seed = (float) rand() / (RAND_MAX);
    }else{
        scan_float("random seed",&r_seed,0,1);         
        printf("Enter the maximum number of generations ---------> "); scanf("%d",&Gmax);
        printf("Enter the population size ---------> "); scanf("%d",&pop_size);        
        scan_float("probability of crossover range(0, 1)",&Pc,0,1);
        scan_float("mutation rate range(0, 1)",&Pm,0,1);                  
        printf("Enter the printing format (suggested \"2\")\n");
        printf("\t3 for print all details; all generation and statistics\n");
        printf("\t2 for print statistics in each generation (omit the generation detail)\n");
        printf("\t1 for print only the best individual of each generation\n");        
        printf("\t0 for print only best ever individual\n");
        printf("------------> ");         
        scanf("%d",&print_arg);        
    }

    /* Perform the memory allocation */
    memory_allocation();

    /* Initialize random number generator */
    randomize();

    // initialization variables
    best_ever.fitness = 0;
    best_ever.generation = 0;
    tourn_size = 2;  /* Use binary tournament selection */
    lb_ub1[0] = 0;  //xi ∈ [-2.3, 2.3], i = 1, 2                    
    lb_ub1[1] = 10;    
    penalty_vec[0] = 5;
    penalty_vec[1] = 1;       
    double max_penalty = penalty_vec[0]+penalty_vec[1];    
    for (int i = 0; i < 2; i++){
        penalty_vec[i] = penalty_vec[i] / max_penalty; //normalize each penalty_value
    }        

    /* Initialize the populations and report statistics */
    initialize_pop();    
    statistics(old_pop);
    initial_report();

    struct individual *temp;    
    for (gen = 1; gen <= Gmax; gen++){ // Iterate the given number of generations  {
        // Create a new generation 
        create_generation();
        // Compute fitness statistics on new populations 
        statistics(new_pop);
        // Report results for new generation 
        generation_report();
        // Advance the generation 
        temp = old_pop;
        old_pop = new_pop;
        new_pop = temp;
    }

    //report the best individual
    best_ever_report();    

    free_all();
    return 0;
}

/* Define randomly the population for generation # 0 */
void initialize_pop() {    
    for (int i = 0; i < pop_size; i++) {                
        // generate chromosome         
        for (int j = 0; j < VAR; j++){            
            old_pop[i].chrom[j] = rnd_real(0, 2);          
        }
        //decode( &old_pop[i] );
        obj_func( &old_pop[i] );
        old_pop[i].parent[0] = 0; /* Initialize parent info */
        old_pop[i].parent[1] = 0;        
    }

    // initialize best_ever
    best_ever.chrom = old_pop[0].chrom;    
    best_ever.fitness = old_pop[0].fitness;    
    best_ever.f_x = old_pop[0].f_x;    
}

/* Perform all the work involved with the creation of a new generation of chromosomes */
void create_generation() {
    int i, mate1, mate2;
    
    /* initialize crossover, mutation and inversion counters */
    n_mutation = 0;
    n_cross = 0;    

    /* perform any preselection actions necessary before generation */
    /* Perform a preselection */
	shuffle();
	tourn_pos = 0;
    
    /* perform any preselection acctions before generation  */
    // calculate the mutation_constant part of the delta mutation
    //∆(t,y) = y ∗ (1 - r^(1-T)^b )
    // b = 5
    mutation_constant = pow(1-(gen/Gmax),5);

    // crossover and mutation
    for(i = 0; i < pop_size - 1; i+=2){            
        /* pick a pair of mates */        
        mate1 = selection();
        mate2 = selection();

        // /* Crossover */        
        crossover( mate1, mate2, i, i+1 );

        // /* mutation */
        mutation( &new_pop[i] );
        mutation( &new_pop[i+1] );
       
        obj_func( &new_pop[i] );
        obj_func( &new_pop[i+1] );
    }

    //Elitism, replace the best individual of the last generation into the new one
    copy_bits(new_pop[0].chrom, best_ind_gen.chrom, 0, VAR);            
    obj_func( &new_pop[0] );    
}

/* Calculate population statistics */
void statistics(struct individual *pop) {
    
    fitness_sum = 0.0;
    fitness_min = pop[0].fitness;
    fitness_max = pop[0].fitness;
    best_ind_gen.chrom = pop[0].chrom;
    best_ind_gen.fitness = pop[0].fitness;

    /* Loop for max, min */
    for (int i = 0; i < pop_size; i++) {
        fitness_sum = fitness_sum + pop[i].fitness; /* Accumulate */        
        
        if (pop[i].fitness > fitness_min){
            fitness_min = pop[i].fitness; /* New minimum */            
        }
        if (pop[i].fitness < fitness_max) {
            fitness_max = pop[i].fitness; /* New maximum */                                   
            best_ind_gen.chrom = pop[i].chrom;
            best_ind_gen.fitness = pop[i].fitness;            
            best_ind_gen.f_x = pop[i].f_x;            

            /* Define new global best_ever-fit individual */
            if (pop[i].fitness < best_ever.fitness) {                
                copy_bits(best_ever.chrom, pop[i].chrom, 0, VAR);
                best_ever.fitness = pop[i].fitness;                
                best_ever.f_x = pop[i].f_x; 
                best_ever.generation = gen;
            } 
        }
    }
    /* Calculate average */
    avg = fitness_sum / pop_size;
}

/* Implementation of a tournament selection process */
int selection(){    
    int pick, winner, i;    

	/* If remaining members not enough for a tournament, then reset list */
	if ((pop_size - tourn_pos) < tourn_size){
		shuffle();
		tourn_pos = 0;
	}

	/* Select tournament size structures at random and conduct a tournament */
	winner = tourn_list[tourn_pos];

	for (i=1; i < tourn_size; i++){
		pick=tourn_list[i+tourn_pos];
		if(old_pop[pick].fitness < old_pop[winner].fitness) 
            winner=pick;
	}

	/* Update tournament position */
	tourn_pos += tourn_size;
	return(winner);
}

/*One point crossover
Se trata realmente de la cruza de un punto aplicada a vectores de números reales:
Ejemplo: Dados
P1 = <3.2, 1.9, | − 0.5>
P2 = <2.9, 0.8, |1.4>
Los hijos serı́an:
H1 = <3.2, 1.9, 1.4>
H2 = <2.9, 0.8, −0.5>
*/
void one_point_crossover(struct individual *child1, struct individual *child2, struct individual *parent1, struct individual *parent2){
    /* Define the crosspoint between 1 and length-2 */ 
    int cross_point = rnd_int(1, VAR);    
    
    // do the one point crossover
    copy_bits( child1->chrom, parent1->chrom, 0, cross_point );
    copy_bits( child1->chrom, parent2->chrom, cross_point, VAR );        
    copy_bits( child2->chrom, parent2->chrom, 0, cross_point );
    copy_bits( child2->chrom, parent1->chrom, cross_point, VAR );
}

/* Cross 2 parent strings, place in 2 child strings */
void crossover( int parent1_idx, int parent2_idx, int child1_idx, int child2_idx){    
    
    struct individual *child1 = &new_pop[child1_idx];
    struct individual *child2 = &new_pop[child2_idx];    
    struct individual *parent1 = &old_pop[parent1_idx];
    struct individual *parent2 = &old_pop[parent2_idx];

    /* Do crossover with probability Pc */
    if (flip(Pc)) {        
        one_point_crossover(child1, child2, parent1, parent2);
        child1->parent[0] = child2->parent[0] = parent1_idx;
        child1->parent[1] = child2->parent[1] = parent2_idx;    
        n_cross+=2; // increase the croosover count.       
    } else {          
        // copy exactly, parents to children
        copy_bits( child1->chrom, parent1->chrom, 0, VAR );
        copy_bits( child2->chrom, parent2->chrom, 0, VAR );
        child1->parent[0] = child2->parent[0] = -1;
        child1->parent[1] = child2->parent[1] = -1;            
    }          
}

// No uniform mutation 
void mutation(struct individual *ind){
    int r;
    for (int i = 0; i < VAR; i++){
        /* Do mutation with probability Pm */
        if (flip(Pm)){
            r = flip(.5);            
            if(r)
                ind->chrom[i] += (lb_ub1[1] - ind->chrom[i]) * (1-pow(rnd_real(0,1),mutation_constant));
            else
                ind->chrom[i] -= (ind->chrom[i] - lb_ub1[0]) * (1-pow(rnd_real(0,1),mutation_constant));                

            n_mutation++; // increase the mutation count.    
        }
    }
}


/*
ρ ≈ 99.9973
v = 20
n = 2 (1 linear inequality, 1 nonlinear inequality; h1 almost active (−10−8))
nonlinear objective function
f(x) =  G2
// restrictions
h1(x) = 0.75 − Qvi=1toV xi ≤ 0
h2(x) = Σi=1toV xi − 7.5v ≤ 0
xi ∈ [0, 10], i = 1, . . . , v
best known value from [163
best known f(x∗) = −0.8036
*/
void obj_func(struct individual *ind){    
    int i;
    long double g1, g2;     
    double term1, term2, term3, cos_v;
    
    //function    
    term1 = term3 = 0;
    term2 = 1;
    for (i = 0; i < 20; i++){
        cos_v = cos(ind->chrom[i]) * cos(ind->chrom[i]);
        term1 += cos_v * cos_v;
        term2 *= cos_v;
        term3 += (i+1)*pow(ind->chrom[i],2); 
    }
    ind->f_x = fabs((term1 - (2*term2) ) / sqrt(term3));

    //restrictions     
    g1 = ind->chrom[0];
    g2 = ind->chrom[0];    
    for (int i = 1; i < VAR; i++){
        g1 *= ind->chrom[i];
        g2 += ind->chrom[i];        
    }
    
    g1 = 0.75-g1;
    g2 = g2-(7.5*VAR);   
    //printf("Entro: %lf\n",g1);
    
    // penalty process                    
    if(g1 > 0) ind->penalty = get_penalty(g1, 0, 0.75, penalty_vec[0]);                             
    if(g2 > 0) ind->penalty += get_penalty(g2, 0, 150, penalty_vec[1]);    

    // normalize the individual penalties and afect the fitness value    
    ind->fitness = ind->f_x * ind->penalty;
    
}

// normalize the penalty
double get_penalty(double value, double lower, double higher, double max_penalty){
    if(value >= higher)
        return max_penalty;        
    
    // x = linf + x' * (lsup-linf)/(2^L - 1)    
    return (value * max_penalty) / higher-lower;            
}

//---------------------------------
//       Auxiliar functions
//---------------------------------

/* Shuffle the tournament list at random */
void shuffle(){
	int i, rand1, rand2, temp;
	
    for (i=0; i < pop_size; i++) 
        tourn_list[i]=i;

	for (i=0; i < pop_size; i++){
		rand1=rnd_int(i,pop_size-1);
		rand2=rnd_int(i,pop_size-1);
		temp=tourn_list[rand1];
		tourn_list[rand1]=tourn_list[rand2];
		tourn_list[rand2]=temp;
	}
}

/* Copy a range of allel, from source to destiny */
void copy_bits(double *dest, double *source, int begin, int end){
    for (int i = begin; i < end; i++){
        dest[i] = source[i];
    }    
}

void scan_float(char *name, float *value, float from_v, float to_v){           
    do{
        printf("Enter the %s, in range %3.1f to %3.1f ------------> ", name, from_v, to_v);
        scanf("%f", value);
    }while( *value < from_v || *value > to_v );
}