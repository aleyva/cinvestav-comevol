
/* Print the initial report of the parameters given by the user */
void initial_report() {
    printf("\nParameters used with the Genetic Algorithm \n");
    printf(" random seed                    =   %3.2f\n", r_seed);        
    printf(" Maximum number of generations  =   %d\n", Gmax);
    printf(" Total population size          =   %d\n", pop_size);        
    printf(" Crossover probability          =   %3.2f\n", Pc);
    printf(" Mutation probability           =   %3.2f\n", Pm);    
    printf(" Penalty factor                 =   %3.2f\n\n", Pen_f);    
}

/* Write the population report on the screen */
void generation_report(){    
    if(print_arg >= 3){               
        printf("___________________________________________________________________________________________________________________________________\n");
        printf(" POPULATION  REPORT  \n");
        printf(" Generation # %3d \t\t\t\t\t\t      Generation # %3d\n", gen-1, gen);
        printf(" num  chromosome                          f_x      penalty  fitness || ");
        printf("parents   chromosome                           f_x      penalty  fitness\n");        
        printf("___________________________________________________________________________________________________________________________________\n\n");

        write_pop();
    }

    /* write the summary statistics in global mode  */    
    if(print_arg >= 1){        
        printf("___________________________________________________________________________________________________________________________________\n");
        printf("Resume Generation # %d\n", gen);
        if(print_arg >= 2){
            printf("  Accumulated Statistics:\n");          
            printf("    Total Crossovers = %d, Total Mutations = %d\n", n_cross, n_mutation);
            printf("    min = %6.1lf   max = %6.1lf   avg = %6.1lf   sum = %6.1lf\n", fitness_min, fitness_max, avg, fitness_sum);
        }
        printf("  Best individual in this generation\n"); 
        write_best(best_ind_gen); printf("\n");       
    }else{
        if(gen%1000 == 0)
            printf("Generation # %d\n", gen);
    }
}

void best_ever_report(){    
    printf("\n|-----------------------------------------------------------------|\n");    
    printf("   Global best Individual so far, Generation # %d: \n", best_ever.generation);
    write_best( best_ever );        

    double x1, x2, x3, x4, x5;
    double h1, h2, h3;
    x1 = best_ever.chrom[0];
    x2 = best_ever.chrom[1];
    x3 = best_ever.chrom[2];
    x4 = best_ever.chrom[3];
    x4 = best_ever.chrom[4];
    
    //restrictions
    h1 = pow(x1,2)+pow(x2,2)+pow(x3,2)+pow(x4,2)+pow(x5,2)-10;
    h2 = x2*x3 - 5*x4*x5;
    h3 = pow(x1,3)+pow(x2,3)+1;
    
    printf("  Restrictions\n");
    printf("\th1 = %lf  h2 = %lf  h3 = %lf\n", h1, h2, h3);    
    printf("|-----------------------------------------------------------------|\n\n");    

}

/* Give the appropriate format to the several values that get printed */
void write_pop() {
    struct individual *pind;
    int j;
    int type_of_print = 1; 

    for (j = 0; j < pop_size; j++) {
        printf("%3d) ", j);

        // Old string 
        pind = & (old_pop[j]);            
        write_line(type_of_print, pind);

        // New string 
        pind = &(new_pop[j]);            
        printf("(%3d,%3d) ", pind->parent[0], pind->parent[1]);
        write_line(type_of_print, pind); 

        printf("\n");
    }
}

void write_line(int type_of_print, struct individual *pind){
    switch(type_of_print){
        case 1: 
            write_chrom(pind->chrom);
            printf(" | %9.4lf %5.2lf %9.4lf || ", pind->f_x, pind->penalty, pind->fitness);
            break;                        
        case 2: 
            write_chrom(pind->chrom);
            printf("    %9.4lf | ", pind->fitness);
            break;                        
    }
}

/* Print out each chromosome from the most significant bit to the least significant bit */
void write_chrom(double *chrom){		
    for (int i = 0; i<VAR; i++) {
		printf("%7.3lf",(double)chrom[i]);			
	}
}

void write_best(struct best_ind ind){    
    printf("    ");             
    for (int i = 0; i<VAR; i++) {
		printf(" %10.7lf",ind.chrom[i]);			
	}   
    printf("    Fittest = %10.6lf   f(x*) = %10.6lf\n", ind.fitness, ind.f_x);    
}

/* Use a system call to clear the screen */
void cls(){
    system("clear");
}
