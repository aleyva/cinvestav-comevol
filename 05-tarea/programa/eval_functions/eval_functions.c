/*
gcc functions.c -o functions -lm 

for g2
./functions 2 679.9453, 1026.067, 0.1188764, -0.3962336
for g5
./functions 2 679.9453, 1026.067, 0.1188764, -0.3962336
for g13
./functions 3 -1.717143 1.595709 1.827247 -0.7636413 -0.763645

 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double g2(double *x);
double g5(double x1,double x2,double x3,double x4);
double g13(double x1,double x2,double x3,double x4,double x5);

int main(int argc, char **argv){    
    double f = 0;
    int i = atoi(argv[1]);
    double x[20] = {};
    switch (i){
        case 1:            
            x[0] = atof(argv[2]);
            x[1] = atof(argv[3]);
            x[2] = atof(argv[4]);
            x[3] = atof(argv[5]);
            x[4] = atof(argv[6]);
            x[5] = atof(argv[7]);
            x[6] = atof(argv[8]);
            x[7] = atof(argv[9]);
            x[8] = atof(argv[10]);
            x[9] = atof(argv[11]);
            x[10] = atof(argv[12]);
            x[11] = atof(argv[13]);
            x[12] = atof(argv[14]);
            x[13] = atof(argv[15]);
            x[14] = atof(argv[16]);
            x[15] = atof(argv[17]);
            x[16] = atof(argv[18]);
            x[17] = atof(argv[19]);
            x[18] = atof(argv[20]);
            x[19] = atof(argv[21]);

            f = g2(x);
            printf("Evaluation of function G2 is\n");
            break;
        case 2:            
            f = g5(atof(argv[2]),atof(argv[3]),atof(argv[4]),atof(argv[5]));
            printf("Evaluation of function G5 is\n");
            break;
        case 3:
            f = g13(atof(argv[2]),atof(argv[3]),atof(argv[4]),atof(argv[5]),atof(argv[6]));
            printf("Evaluation of function G13 is\n");
            break;
    }

    printf("f()= %lf\n", f);

    return 0;
}

/*
ρ ≈ 99.9973
v = 20
n = 2 (1 linear inequality, 1 nonlinear inequality; h1 almost active (−10−8))
nonlinear objective function
f(x) =  G2
// restrictions
h1(x) = 0.75 − Qvi=1toV xi ≤ 0
h2(x) = Σi=1toV xi − 7.5v ≤ 0
xi ∈ [0, 10], i = 1, . . . , v
best known value from [163
best known f(x∗) = −0.8036
*/
double g2(double *x){
    double term1, term2, term3, cos_v, f;  
    double g1, g2;  
    
    //function    
    term1 = term3 = 0;
    term2 = 1;
    for (int i = 0; i < 20; i++){
        cos_v = cos(x[i]) * cos(x[i]);
        term1 += cos_v * cos_v;
        term2 *= cos_v;
        term3 += (i+1)*pow(x[i],2); 
    }
    f = fabs((term1 - (2*term2) ) / sqrt(term3));

    //restrictions     
    g1 = x[0];
    g2 = x[0];    
    for (int i = 1; i < 20; i++){
        g1 *= x[i];
        g2 += x[i];        
    }
    g1 = 0.75-g1;
    g2 = g2-(7.5*20); 
    
    printf("g1 = %lf\n", g1);
    printf("g2 = %lf\n", g2);
    printf("f()= %lf\n", f);
    return f;
}

/*
ρ ≈ 0.0000
v = 4
n = 5 (2 linear inequalities, 3 nonlinear equalities; g1, g2, g3 are active)
nonlinear objective function
f(x) = 3x1 + 0.000001x1³ + 2x2 + (0.000002/3)x2³
//restricciones
h1(x) = −x4 + x3 − 0.55 ≤ 0
h2(x) = −x3 + x4 − 0.55 ≤ 0
g1(x) = 1000 sin(−x3 − 0.25) + 1000 sin(−x4 − 0.25) + 894.8 − x1 = 0
g2(x) = 1000 sin(x3 − 0.25) + 1000 sin(x3 − x4 − 0.25) + 894.8 − x2 = 0
g3(x) = 1000 sin(x4 − 0.25) + 1000 sin(x4 − x3 − 0.25) + 1294.8 = 0
xi ∈ [0, 1200], i = 1, 2
xi ∈ [−0.55, 0.55], i = 3, 4
best known x∗ = (679.9453, 1026.067, 0.1188764, −0.3962336)
f(x*) = 5126.4981
*/

double g5(double x1,double x2,double x3,double x4){
    printf("x1= %lf\n", x1);
    printf("x2= %lf\n", x2);
    printf("x3= %lf\n", x3);
    printf("x4= %lf\n", x4);    
    
    double f, h1, h2, g1, g2, g3;

    //function
    f = 3*x1 + 0.000001*pow(x1,3) + 2*x2 + (0.000002/3)*pow(x2,3);
    
    //restrictions
    h1 = -x4 + x3 - 0.55;
    h2 = -x3 + x4 - 0.55;
    g1 = 1000*sin(-x3 - 0.25) + 1000*sin(-x4 - 0.25) + 894.8 - x1;
    g2 = 1000*sin(x3 - 0.25) + 1000*sin(x3 - x4 - 0.25) + 894.8 - x2;
    g3 = 1000*sin(x4 - 0.25) + 1000*sin(x4 - x3 - 0.25) + 1294.8;
    printf("h1= %lf\n", h1);
    printf("h2= %lf\n", h2);
    printf("g1= %lf\n", g1);
    printf("g2= %lf\n", g2);
    printf("g3= %lf\n", g3);
    
    return f;
}

/* function 
f(x) = exp(x1x2x3x4x5)
// restrictions
h1(~x)=x1²+x2²+x3²+x4²+x5²−10=0
h2(~x)=x2x3−5(x4x5)=0
h3(~x)=x1³+x2³+1=0
xi ∈ [−2.3, 2.3], i = 1, 2
xi ∈ [−3.2, 3.2], i = 3, 4, 5
x∗ = (−1.717143, 1.595709, 1.827247, −0.7636413, −0.763645)
f(x∗) = 0.0539498
*/
double g13(double x1,double x2,double x3,double x4,double x5){    
    printf("x1= %lf\n", x1);
    printf("x2= %lf\n", x2);
    printf("x3= %lf\n", x3);
    printf("x4= %lf\n", x4);
    printf("x5= %lf\n", x4);
    
    double term1, term2, term3, f;

    //function
    f = exp(x1 * x2 * x3 * x4 * x5);    
    
    //restrictions    
    term1 = term2 = term3 = 0;    
    term1 = pow(x1,2)+pow(x2,2)+pow(x3,2)+pow(x4,2)+pow(x5,2)-10;
    term2 = x2*x3 - 5*x4*x5;
    term3 = pow(x1,3)+pow(x2,3)+1;
    printf("h1= %lf\n", term1);
    printf("h2= %lf\n", term2);
    printf("h3= %lf\n", term3);


    return f;
}