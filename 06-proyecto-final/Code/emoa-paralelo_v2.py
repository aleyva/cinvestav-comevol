import pygmo as pg
import matplotlib.pyplot as plt 
from pygmo import hypervolume

#########################
#       parameters
#########################

# Gmax = 100
# evolve_step = 1
# evolve_times = 20
Gmax = 20
evolve_step = 10
evolve_times = 40
calculate_hypervol_each_evol = True #{true or false}
plot_v = False

# island parameter
pop_size=30  # {6, 10, 15, 21, 22, 28, 36... }
islands=4  
topology=pg.fully_connected(n=0, w=1)

#emoa parameters
neighbours=2

# wfg parameters
# prob_id (int) – WFG problem id
# dim_dvs (int) – decision vector size
# dim_obj (int) – number of objectives
# dim_k (int) – position parameter
prob_id=1
dim_dvs=44
dim_obj=3
dim_k=24

#best_pop = [hyper, pop]
best_pop_hyper = 0
best_pop = []

#########################
#       functions
#########################

# print the hypervol in each island
def print_hypervol_each_isl(archi):
    i = 1
    hyper_sum = 0
    hyper_max = [-1, -1]
    hyper_min = [-1, -1]
    pop = []
    print('   hypevolume info: ')
    for isl in archi:        
        hv = pg.hypervolume(isl.get_population())
        ref_point = hv.refpoint(offset = 1)
        
        hypervol = hv.compute(ref_point)
        hyper_sum += hypervol
        if(i == 1):
            hyper_max = [hypervol, i]
            hyper_min = [hypervol, i]
            pop = isl.get_population() 
        else:
            if(hypervol > hyper_max[0]):
                hyper_max = [hypervol, i]
                pop = isl.get_population() 
            elif (hypervol < hyper_min[0]):
                hyper_min = [hypervol, i]                
        
        print('     hypervolume of island:', i, ' ', hypervol )
        i += 1
    print('   hypevolume min: ', hyper_min[0], ' in island ', hyper_min[1])
    print('   hypevolume best: ', hyper_max[0], ' in island ', hyper_max[1])    
    print('   hypevolume avg: ', hyper_sum/islands)    
    print('   best individual: ', pop)
    
    # return [ hyper_max[0], pop ]
    return [hyper_max[0], pop]

# print pareto info
def print_pareto_info(archi):
    i = 1
    print('   Pareto info: ')
    total_pop = []
    for isl in archi:      
        pop = isl.get_population()        
        # ndf (list of 1D NumPy int array): the non dominated fronts
        # dl (list of 1D NumPy int array): the domination list
        # dc (1D NumPy int array): the domination count
        # ndr (1D NumPy int array): the non domination ranks
        
        points = pop.get_f()
        idx = pg.non_dominated_front_2d(points)       
        x = [points[i][0] for i in idx] 
        y = [points[i][1] for i in idx]         
        plt.scatter(x, y)
        
        i += 1

    plt.title("Pareto Front")
    plt.show()
        
        

def print_variables_info():
    print('######################')
    print('# Variables information')
    print('######################')
    print('  Gmax', Gmax)
    print('  evolve_step', evolve_step)
    print('  evolve_times', evolve_times)

    print('######################')
    print('# Variables information')
    print('######################')
    print('  function WFG', prob_id)    
    print('  decision vector size', dim_dvs)
    print('  dimension de las funciones objetivo', dim_obj)

    print('######################')
    print('# Islands information')
    print('######################')
    print('  Islands', islands)
    print('  Pop_size per island', pop_size)
    print('  Topology', archi.get_topology())
    print('  Migration type', archi.get_migration_type())



def plot_one_island(archi, isl_n):
    i = 1
    for isl in archi:   
        pop = isl.get_population()     
        if(isl_n==0):
            ax = pg.plot_non_dominated_fronts(pop.get_f())
             
        elif(isl_n==i):
            ax = pg.plot_non_dominated_fronts(pop.get_f())            
             
        i+=1
    plt.ylim([0,6]) 
    plt.title("ZDT1: random initial population")
    plt.title("Pareto front all islands")  
    plt.show() 

def plot_one_island2(archi, isl_n):
    i = 1
    for isl in archi:   
        pop = isl.get_population()     
        if(isl_n==0):
            ndf, dl, dc, ndl = pg.fast_non_dominated_sorting(pop.get_f())
            ax = ndf 
        elif(isl_n==i):
            ndf, dl, dc, ndl = pg.fast_non_dominated_sorting(pop.get_f())
            ax = ndf 
        i+=1
    plt.title("Pareto front all islands")  
    plt.show() 
        
def plot_islands(archi, grid):
    i = 1
    fig, axs = plt.subplots(2, 2)
    for isl in archi:        
        pop = isl.get_population()
        if(i==1):
            axs[0, 0] = pg.plot_non_dominated_fronts(pop.get_f()) 
            axs[0, 0].set_title('Axis [0,0]')
        elif(i==2):
            axs[0, 1] = pg.plot_non_dominated_fronts(pop.get_f()) 
        elif(i==3):
            axs[1, 0] = pg.plot_non_dominated_fronts(pop.get_f()) 
        else:
            axs[1, 1] = pg.plot_non_dominated_fronts(pop.get_f()) 
        i+=1
        
    for ax in axs.flat:
        ax.set(xlabel='x-label', ylabel='y-label')

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    plt.show()        

#########################
#         main
#########################


# constructiong the problem
# prob = pg.zdt(prob_id = 1)
prob = pg.wfg(prob_id=prob_id,dim_dvs=dim_dvs,dim_obj=dim_obj,dim_k=dim_k)

# define the algorithm 
algo = pg.algorithm(pg.moead(gen=Gmax,neighbours=neighbours))

# Instantiate an archipelago with n=islands, 'pop_size' individuals
archi = pg.archipelago(n=islands, algo=algo, prob=prob, pop_size=pop_size, t=topology)

print_variables_info()


print('########################')
print('# Starting the algorithm')
print('########################')


print('Times evolved 0 ...')
print_hypervol_each_isl(archi)


converg = []
for i in range(evolve_times):    
    print('Times evolved ', i+1, ' ...')
    # Run the evolution in parallel.
    archi.evolve(evolve_step)
    # Wait for the evolutions to be finished
    archi.wait()    
    
    # calculate the hypervol of each island   
    if(calculate_hypervol_each_evol):
        [hyper_max, pop] = print_hypervol_each_isl(archi)        
        converg.append([i, hyper_max])        
        if(hyper_max> best_pop_hyper):
            best_pop_hyper = hyper_max
            best_pop = pop

    
    #print_pareto_info(archi)        


# print_pareto_info(archi)

if(plot_v):
    # print all the islands = 0
    # print a especific islan = n
    plot_one_island(archi, 0)

    x = []
    y = []
    for i in converg:
        x.append(i[0])
        y.append(i[1])
    plt.plot(x, y, 'k')
    plt.title("Convergence Graph")  
    plt.xlabel("Evolutions")
    plt.ylabel("Hypervolume")
    plt.show()


print('########################')
print('# Best Values')
print('########################')

print('   Best hypevolume: ', hyper_max)    
print('   best individual: ', best_pop)

print_pareto_info(archi)

