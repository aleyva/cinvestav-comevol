import pygmo as pg
# Instantiates a 4-objectives problem
prob = pg.wfg(prob_id=1,dim_dvs=5,dim_obj=3,dim_k=4)
# udp = zdt(prob_id = 1)
pop = pg.population(prob=prob, size = 120, seed = 3453412)
# Construct the hypervolume object
# and get the reference point off-setted by 10 in each objective
hv = pg.hypervolume(pop)
offset = 5
ref_point = hv.refpoint(offset = 0.1)
print('hypervolume', hv.compute(ref_point) )

# Evolve the population some generations
algo = pg.algorithm(pg.moead(gen=2000))
pop = algo.evolve(pop)
# Compute the hypervolume indicator again.
# This time we expect a higher value as SMS-EMOA evolves the population
# by trying to maximize the hypervolume indicator.
hv = pg.hypervolume(pop)
ref_point = hv.refpoint(offset = 0.1)
print('hypervolume', hv.compute(ref_point) )
