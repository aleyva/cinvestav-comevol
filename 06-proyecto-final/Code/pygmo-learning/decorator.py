# ^\.\.\.\s?
import pygmo as pg
import matplotlib.pyplot as plt 

# The problem
prob = pg.problem(pg.rosenbrock(dim = 10))
# The initial population
pop = pg.population(prob, size = 20)
# The algorithm (a self-adaptive form of Differential Evolution (sade - jDE variant)
algo = pg.algorithm(pg.sade(gen = 1000))
# The actual optimization process
pop = algo.evolve(pop)
# Getting the best individual in the population
best_fitness = pop.get_f()[pop.best_idx()]
print(best_fitness) 

# We set the verbosity to 100 (i.e. each 100 gen there will be a log line)
algo.set_verbosity(100)

# We perform an evolution
pop = pg.population(prob, size = 20)
pop = algo.evolve(pop) 

uda = algo.extract(pg.sade)
log = uda.get_log()

plt.semilogy([entry[0] for entry in log],[entry[2]for entry in log], 'k--') 
plt.show() 