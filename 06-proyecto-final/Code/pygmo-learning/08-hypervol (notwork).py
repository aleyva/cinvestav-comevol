from pygmo import *
from matplotlib import pyplot as plt 

udp = dtlz(prob_id = 1)
pop = population(prob = udp, size = 105)
algo = algorithm(moead(gen = 100))
for i in range(10):
    pop = algo.evolve(pop)
    print(udp.p_distance(pop)) 


hv = hypervolume(pop)
hv.compute(ref_point = [1.,1.,1.]) 

udp.plot(pop) 
plt.title("DTLZ1 - MOEAD - GRID - TCHEBYCHEFF") 
plt.show()

