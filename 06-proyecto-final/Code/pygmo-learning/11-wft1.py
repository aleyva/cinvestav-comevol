import pygmo as pg
import matplotlib.pyplot as plt 
from pygmo import hypervolume



# prob_id (int) – WFG problem id
# dim_dvs (int) – decision vector size
# dim_obj (int) – number of objectives
# dim_k (int) – position parameter
prob = pg.wfg(prob_id=1,dim_dvs=5,dim_obj=3,dim_k=4)
# udp = zdt(prob_id = 1)
pop = pg.population(prob=prob, size = 29, seed = 3453412)
# define the algorithm 
algo = pg.algorithm(pg.moead(gen=2000,neighbours=2))

# ndf (list of 1D NumPy int array): the non dominated fronts
# dl (list of 1D NumPy int array): the domination list
# dc (1D NumPy int array): the domination count
# ndr (1D NumPy int array): the non domination ranks
ndf, dl, dc, ndl = pg.fast_non_dominated_sorting(pop.get_f()) 

# plot
if(0):
    fig, ax = plt.subplots()
    ax = pg.plot_non_dominated_fronts(pop.get_f()) 
    #ax.plot(x, y)    
    ax.set_title('A single plot')    
    # plt.ylim([0,1.5]) 
    plt.title("wfg1: random initial population")  
    plt.show()    
    
        
# evolve the algorithm
pop = algo.evolve(pop)

# plot again
if(0):
    ax = pg.plot_non_dominated_fronts(pop.get_f()) 
    plt.title("wfg1: ... and the evolved population")  
    plt.show() 

ndf, dl, dc, ndl = pg.fast_non_dominated_sorting(pop.get_f())
print(ndf) 


# print(prob.p_distance(pop))

# pop = algo.evolve(pop)
# print(udp.p_distance(pop) )




#########################
# hypervolume calculation
#########################
# hv = hypervolume(pop)
# # reference point
# ref_point = [2,2,2]
# print('hypervolume:', hv.compute(ref_point))


## The second way of construction uses an explicit representation of coordinates for the input point set:
# from numpy import array
# hv = hypervolume(array([[1,0],[0.5,0.5],[0,1]]))
# hv_from_list = hypervolume([[1,0],[0.5,0.5],[0,1]])
