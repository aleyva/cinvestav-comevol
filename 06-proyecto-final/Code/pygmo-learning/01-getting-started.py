import pygmo as pg

# 1 - Instantiate a pygmo problem constructing it from a UDP
# (user defined problem).
prob = pg.problem(pg.schwefel(30))

# 2 - Instantiate a pagmo algorithm
algo = pg.algorithm(pg.sade(gen=100))

topology = pg.fully_connected(n=0, w=1)
# 3 - Instantiate an archipelago with 16 islands having each 20 individuals
archi = pg.archipelago(16, algo=algo, prob=prob, pop_size=20, t=topology)

# 4 - Run the evolution in parallel on the 16 separate islands 10 times.
archi.evolve(10)

# 5 - Wait for the evolutions to be finished
archi.wait()

sum = 0
# 6 - Print the fitness of the best solution in each island
for isl in archi:    
    # if(sum==0):
    #     print(isl.get_population())
    champion = isl.get_population().champion_f
    sum += champion
    print (champion)
# print ('avg ', sum/16)

# res = [isl.get_population().champion_f for isl in archi]
# print(res)