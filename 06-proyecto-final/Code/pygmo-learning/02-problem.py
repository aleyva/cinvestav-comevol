import pygmo as pg

# prob_id (int) – WFG problem id
# dim_dvs (int) – decision vector size
# dim_obj (int) – number of objectives
# dim_k (int) – position parameter
prob = pg.problem(pg.wfg(prob_id=1,dim_dvs=5,dim_obj=3,dim_k=4))
print(prob) 

# All of the information contained in the problem print out can be retrieved using the appropriate methods, for example:
print(prob.get_fevals())
#Lets check how a fitness computation increases the counter:
prob.fitness([1,2,3,4,5])
print(prob.get_fevals())

udp = prob.extract(pg.wfg)
print(type(udp))

udp = prob.extract(pg.rastrigin)
udp is None
