import pygmo as pg
prob = pg.problem(pg.rosenbrock(dim = 4))
pop1 = pg.population(prob)
pop2 = pg.population(prob, size = 5, seed= 723782378)

print(len(pop1))
print(pop1.problem.get_fevals())
print(len(pop2))
print(pop2.problem.get_fevals())

print(pop2) 