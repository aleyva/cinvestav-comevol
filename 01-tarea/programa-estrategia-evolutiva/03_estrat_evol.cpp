
/*=============================================================================
 |   Assignment:  ASSIGNMENT NUMBER AND TITLE
 |
 |       Author:  Antonio Leyva
 |     Language:  C
 |   To Compile:  g++ 03_estrat_evol.cpp -o 03_estrat_evol -lm
 |       To Run:  ./03_estrat_evol 5000 .87
 |        Class:  Computación Evolutiva
 |   Instructor:  Carlos Coello
 |     Due Date:  28-may-2019
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Programa que encuentra los optimos para la funcion 
 |                f(x1,x2) = [1.5 − x1(1 − x2 )]² + [(2.25 − x1 (1 − x2² )]² + [2.625 − x1 (1 − x2³)]²
 |
 |        Input:  g_max y c
 |
 *===========================================================================*/


// compilar
// gcc programa2.c -o programa2 -lm
// ejecutar
// ./programa2


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "random_gen.c"

/*     
	FUNCTION f
	purpose: evaluar la función f(x1,x2) = [1.5 − x1(1 − x2 )]² + [(2.25 − x1 (1 − x2² )]² + [2.625 − x1 (1 − x2³)]²
	parameters: las variables de la funcion x1 y x2			
	 output: devuelve la evaluación de la función
	example: para x1 = 7 y x2 = -3, devuelve 41489.203125
*/
double f(double x1, double x2){
	double term1, term2, term3;
	term1 = term2 = term3 = 0;
	term1 = pow(  1.5 - x1 * (1 - x2)         , 2 );
	term2 = pow( 2.25 - x1 * (1 - pow(x2,2) ) , 2 );
	term3 = pow(2.625 - x1 * (1 - pow(x2,3) ) , 2 );
	return term1 + term2 + term3; 
}

/*     
	FUNCTION new_var
	purpose: regresar un nuevo valor en el rango -5.0 <= x <= 10.0, 
				para generarlo usa el vector mutacion xi = xi + σ[t] × Ni(0, 1)
	parameters: x y sigma
	output: devuelve el nuevo valor de x en el rango mencionado
*/
double new_var (double x, double sigma){	
	//calcula el nuevo valor de x en x_new
	double x_new = x + sigma * N(0, 1);
	//verifica que x_new esté dentro del rango -5.0 <= x_new <= 10.0,
	//de lo contrario realiza un ajuste con la funcion modulo fmod,
	//la funcion "fmod" permite realizar operaciones de modulo con número reales.
	//Para que el nuevo valor generado "x_new" quede entre el rango -5.0 <= x <= 10.0, 
	//primero se le suma 5 a "x_new" para que el rango esté solo en valores positivos, 
	//posteriormente se aplica el modulo de 15 ya que el nuevo rango es 0 <= x <= 15.0
	//y finalmente se le vuelve a restar 5 para devolverlo al rango original.
  if(x_new < -5.0 || x_new > 10){				
		x_new = std::fmod(abs(x_new+5), 15) - 5;		
	}
	return x_new;
}	 

/*     
	FUNCTION flip
	purpose: devuelve 1 si debe mutar o 0 si no debe mutar, evaluando la ocurrencia sobre la constante c				
	parameters: c, constante de mutación
	output: devuelve 1 o 0
*/
int flip(double c){	    
    // printf("c %lf, r %lf, %d\n", c, r, c > r);
    if(c > randreal())
		return 1;
	else
		return 0;
}


int main(int argc, char **argv){
	//espera que el programa tenga 2 argumentos, <g_max> y <c>
	if (argc !=3 ) {
		printf("Argumentos esperados <g_max> <c>\n");
		exit(EXIT_FAILURE);
	}

	int generation; 	    //generation = contador de generaciones	
	int g_max;				//cota superior de generaciones, g_max >= 10
	int n_var = 2; 			//n_var = número de variables	
	int ps = 0;				//frecuencia de exito de las mutaciones
	int remplazo = 0;		//conteo de veces que un hijo sustituye al padre
	float sigma = 3.0;  	//sigma
	float c;     		    //constante de mutación, valores entre .817 y 1.0
	double x1, x2, x1_best, x2_best;    //variables, siempre en el rango -5.0 <= x1,x2 <= 10.0
	double fitness, fitness_best;       //fitness

	// lee los argumentos
    g_max = atoi(argv[1]);
    if(g_max < 10)
        g_max = 10;
	c = atof(argv[2]);		
    if(c < 0 || c > 1)
        c = .817;

        
	//inicializar semilla de aleatorios	
	initrandom(time(0));

	//inicializa variables, se sabe que el punto optimo es f(3, .5)
	// x1 = 3;
	// x2 = .5;	    
	x1 = randint(-5,10);	
	x2 = randint(-5,10);
	
    //Evaluar f (x)
	fitness_best = f(x1, x2);

    //abre el archivo para escritura
    std::ofstream myfile;
    std::stringstream ss;        
    ss << "output-gmax_" << g_max << "-c_" << c << "-x1_" << x1 << "-x2_" << x2;
    // std::string s = ss.str()    
    myfile.open (ss.str());
	
    //imprime los valores iniciales
    printf("Initial values    x1:%5.2lf, x2:%5.2lf, fitness:%5.3lf\n", x1, x2, fitness_best);

    //establece la precision con la que se imprimirán los decimales en el archivo
    std::cout.precision(6);

	//itera hasta alcanzar el numero maximo de generaciones
    for (generation = 2; generation <= g_max; generation++){
		//mutar el vector xi usando: xi = xi + σ[t] × Ni(0, 1) ∀i ∈ n
        // la mutacion ocurre solo si se da la probabilidad
		if(flip(c))
		    x1 = new_var (x1, sigma);
	    if(flip(c))
		    x2 = new_var (x2, sigma);		
        // x1 = new_var (x1, sigma);
		// x2 = new_var (x2, sigma);
		
		//Evaluar f(x')
		fitness = f(x1, x2);	 
				
        //Guarda el valor de x1, x2 y fitness en archivo
        if(generation %100 ==0 )
			myfile << "   gen: " << generation << " x1: " << x1 << " x2: " << x2 <<" fitness: " << fitness << "\n";

		//Comparar x̄ con x̄' y seleccionar el mejor
		if( fitness < fitness_best ){
			x1_best = x1;
			x2_best = x2;
			fitness_best = fitness;
			remplazo++;
			
            printf("BEST - gen: %4d, x1:%5.2lf, x2:%5.2lf, fitness:%5.3lf\n", generation, x1_best, x2_best, fitness_best);
            //Guarda los mejores valores de x1, x2 y fitness encontrados hasta el momento en el archivo
            myfile << "BEST - gen: " << generation << " x1: " << x1_best << " x2: " << x2_best <<" fitness: " << fitness_best << "\n";
		}

		//calculo de sigma según ps y una 
        if( generation % n_var == 0) {			
			if(ps > .2)
				sigma = (generation - n_var) / c;
			if(ps < .2)
				sigma = (generation - c) * c;
			if(ps == .2)
				sigma = generation - n_var;		
		}else{
			sigma = generation - 1;
		} 
		
        //Cálculo de ps, para calcularla, se registrará como exitosa aquella mutación en la que el hijo reemplace a su padre. 
        //La actualización de ps se efectuará cada 10 · n iteraciones.
		if(generation % 20 == 0){
			ps = remplazo / generation;
		}
		
	}
	//imprime los mejores valores encontrados en consola
    printf("BEST - gen: %4d, x1:%5.4lf, x2:%5.4lf, fitness:%5.4lf\n", generation, x1_best, x2_best, fitness_best);		
    //Guarda los mejores valores encontrados de x1, x2 y fitnesss en archivo
    myfile << "\nBEST - x1_best: " << x1_best << " x2: " << x2_best <<" fitness_best: " << fitness_best << "\n";
	
    //cierra el archivo
    myfile.close();
	return 0;
}



